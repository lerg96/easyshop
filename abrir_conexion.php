<?php 
	// Parametros a configurar para la conexion de la base de datos 
	$host = "localhost";    // sera el valor de nuestra BD 
	$basededatos = "easyshop";    // sera el valor de nuestra BD
	$usuariodb = "root";    // sera el valor de nuestra BD 
	$clavedb = "root";    // sera el valor de nuestra BD

	//Lista de Tablas
	$tabla_db1 = "usuario"; 	   // tabla de usuarios
	$tabla_db2 = "producto"; 	   // tabla de productos
	$tabla_db3 = "productos_despachados"; 	   // ventas actuales
	$tabla_db4 = "productos_vendidos"; 	   // ventas actuales
	$tabla_db5 = "facturas"; 	   // ventas actuales
	$tabla_db6 = "egreso"; 	   // Registro de Egresos
	$tabla_db7 = "imprimir_factura"; 	   //parametros de la factura

	//error_reporting(0); //No me muestra errores, como los de inicializacion de variables
	
	$conexion = new mysqli($host,$usuariodb,$clavedb,$basededatos);


	if ($conexion->connect_errno) {
	    echo "Habla Dostin, Rectifica los datos en el archivo abrir_conexion.php... cualquier duda contactame contacto@dostinhurtado.com";
	    exit();
	}

?>