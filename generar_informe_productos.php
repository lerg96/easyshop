﻿<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Diseño y Publicidad" content="">
    <meta name="Central de Diseño" content="">
    <link rel="icon" href="img/logo.ico">
    <title>Informes Productos</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- Bootstrap core CSS -->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--JavaScrip para las FECHAS-->
  <script>
      $(function(){
        $('.datepicker').datepicker();
      });
    </script>

  </head>
<body>
  <?php
    session_start();
    ob_start();
    include("abrir_conexion.php"); 

    //Si no inicia sesion. ¡Chao papá!
    if($_SESSION['sesion_exito']<>1){header('Location:index.php');} //Si NO inicio sesion, ¡hasta luego!
    if($_SESSION['tipo_usuario']<>"A"){header('Location:index.php');}//Si NO es administrador, Chao mijo!

    

    //Rectifico quien me llama (GET)
    //if(isset($_GET['tipo'])){$tipo=$_GET['tipo'];}
    $tipo_informe="";
    //Rectifico quien me llama (Lista de productos)
    if(isset($_POST['informe_productos'])){$tipo_informe="productos";}


    //en caso de que me llame INGRESOS
    if($tipo_informe=="productos")
    {
        if($_POST['fecha_inicial']=="" || $_POST['fecha_final']=="")//no especifico fechas, agrego la de HOY
        {
          $fecha_inicial=date('m/d/Y');
          $fecha_final=date('m/d/Y');
        }
        else//Envian fechas desde Admin
        {
          $fecha_inicial=$_POST['fecha_inicial'];
          $fecha_final=$_POST['fecha_final'];
        }

        //Configuro las fechas para adaptarlas a SQL (año-mes-dia)
        list($mes, $dia, $year)=explode("/", $fecha_inicial);
        $fecha_inicial = $year."-".$mes."-".$dia; 

        list($mes, $dia, $year)=explode("/", $fecha_final);
        $fecha_final = $year."-".$mes."-".$dia; 
    }
    
     
  ?>
<div class="container">
 <input type="hidden" name="agregar_producto" id="existe" value="1">
  <input type="hidden" name="no_mesa" id="mesa" value="<?php echo $mesa; ?>">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1>INFORME GENERADO</h1>
        <p class="lead"><strong>LISTA DE PRODUCTOS VENDIDOS</strong></p>
        <hr>
      </div>
    </div>
    
    <div class="row">         
      <div class="col-md-3"></div>
      <div class="col-md-6">         
        <div class="well">
        <h2><center><strong>LISTA DETALLADA</strong></center></h2><br>
        
        <center><div class="table-responsive"><table width="80%" class="table table-bordered"> 
          <tr>
            <th width="15%"><center>VENTAS</center></th>
            <th width="50%"><center>DESCRIPCIÓN</center></th>
            <th width="35%"><center>TOTAL VENTAS</center></th>
          </tr>
            <?php
              $total_general=0;

              //Busco en todas los productos existentes
              $resultados = mysqli_query($conexion,"SELECT * from $tabla_db2");
              while($consulta = mysqli_fetch_array($resultados))
              {
                $codigo=$consulta['codigo'];
                $nombre=$consulta['nombre'];
                $total=0;
                $i=0;
                
                $resultados2 = mysqli_query($conexion,"SELECT * from $tabla_db5 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
                while($consulta2 = mysqli_fetch_array($resultados2))
                {
                  $id_factura=$consulta2['id'];

                    //en la tabla de productos vendidos, relaciono los IDs de los productos 
                    $resultados3 = mysqli_query($conexion,"SELECT * from $tabla_db4 WHERE codigo = '$codigo' AND id_factura = '$id_factura'");
                      while($consulta3 = mysqli_fetch_array($resultados3))
                      {
                        $total=$total+$consulta3['precio_total'];
                        //calculo la cantidad de productos que despacho
                        $i=$i+$consulta3['cantidad'];
                      } 
                }

                $total_general=$total_general+$total;

                if(!$total==0)
                {
                  echo '<tr><td><center>'.$i.'</center></td><td>'.$consulta['nombre'].'</td><td>'.number_format($total, 0, ",", ".").'</td></tr>';
                }
              }


              $total=0;
              $i=0;
              
              $resultados = mysqli_query($conexion,"SELECT * from $tabla_db5 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
              while($consulta = mysqli_fetch_array($resultados))
              {
                $id_factura=$consulta['id'];

                //calculo productos agregados manualmente COD:X
                $resultados2 = mysqli_query($conexion,"SELECT * from $tabla_db4 WHERE codigo = 'x' AND id_factura = '$id_factura'");
                while($consulta2 = mysqli_fetch_array($resultados2))
                {
                  $total=$total+$consulta2['precio_total'];
                   $i=$i+$consulta2['cantidad'];
                }
              }

              if(!$total==0)
              {
                echo '<tr><td><center>'.$i.'</center></td><td>OTROS PRODUCTOS</td><td>'.number_format($total, 0, ",", ".").'</td></tr>';
              }
              $total_general=$total_general+$total;

              //muestro el total de TODOS LOS PRODUCTOS
              echo '<tr><td colspan="2"><p class="text-right"><strong>TOTAL GENERAL</strong></p></td><td><strong>'.number_format($total_general, 0, ",", ".").'</strong></td>';
            ?>
        </table></div></center>
        <center><a href="admin.php" class="btn btn-warning btn-lg" role="button">VOLVER</a></center>
        

        </div>
      </div>        
      <div class="col-md-3"></div>
    </div>

  <div class="row">         
    <div class="col-md-1"></div>
    <div class="col-md-10">
    <h2><center>DETALLES</center></h2>
      <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" width="100%">
          
          <?php
          if($tipo_informe=="productos")
          {
            echo 
            '
              <tr>
                <th width="15%"><center>ID FACTURA</center></th>
                <th width="15%"><center>ID PRODUCTO</center></th>
                <th width="5%"><center>MESA</center></th>
                <th width="5%"><center>CANT</center></th>
                <th width="40%"><center>DESCRIPCIÓN</center></th>
                <th width="10%"><center>PRECIO</center></th>
                <th width="10%"><center>TOTAL</center></th>
                
              </tr>
            ';
            $resultados = mysqli_query($conexion,"SELECT * from $tabla_db5 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
            while($consulta = mysqli_fetch_array($resultados))
            {
              //hallo el ID de las facturas en esas fechas
              $id_factura=$consulta['id'];

              //en la tabla de productos vendidos, busco los despachados en dichas facturas 
              $resultados2 = mysqli_query($conexion,"SELECT * from $tabla_db4 WHERE id_factura = '$id_factura'");
              while($consulta2 = mysqli_fetch_array($resultados2))
              {
                echo '
                  <tr>
                    <td><center>'.$consulta2['id_factura'].'</center></td>
                    <td><center>'.$consulta2['codigo'].'</center></td>
                    <td><center>'.$consulta2['mesa'].'</center></td>
                    <td><center>'.$consulta2['cantidad'].'</center></td>
                    <td><center>'.$consulta2['nombre'].'</center></td>
                    <td><center>'.number_format($consulta2['precio_unitario'], 0, ",", ".").'</center></td>
                    <td><center>'.number_format($consulta2['precio_total'], 0, ",", ".").'</center></td> 
                  </tr>
                ';
              }
            }
          }
          ?>
        </table>
      </div>
    </div>
    <div class="col-md-1"></div>
  </div>
</div><!--Containder-->
<?php include("cerrar_conexion.php"); ?>
</body>
  
</html>
