<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Diseño y Publicidad" content="">
    <meta name="Central de Diseño" content="">
    <link rel="icon" href="img/potato.ico">
    <title>Buyme - Administrar Egresos</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- Bootstrap core CSS -->
    <!--<link href="./css/navbar-fixed-top.css" rel="stylesheet">-->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--JavaScrip para las FECHAS-->
  <script>
      $(function(){
        $('.datepicker').datepicker();
      });
    </script>

  </head>
<body>
  <?php
    session_start();
    ob_start();
    include("abrir_conexion.php"); 

    //Si no inicia sesion. ¡Chao papá!
    if($_SESSION['sesion_exito']<>1){header('Location:index.php');} //Si NO inicio sesion, ¡hasta luego!

    $fecha_inicial=date('m/d/Y');
    list($mes, $dia, $year)=explode("/", $fecha);
    $fecha = $year."-".$mes."-".$dia; 

    if(isset($_POST['guardar_egreso']))
    {
      //recibo datos esenciales
      $fecha=$_POST['fecha'];
      $descripcion=$_POST['descripcion'];
      $total=$_POST['egreso'];
 //if(!ctype_digit($mesa))
      if($fecha=="" || $descripcion=="" || !ctype_digit($total))
      {$error=1;} //1 para datos incompletos
      else
      {
        list($mes, $dia, $year)=explode("/", $fecha);
        $fecha = $year."-".$mes."-".$dia; 

        $responsable=$_SESSION['nombre_usuario'];

        mysqli_query($conexion, "INSERT INTO $tabla_db6 (fecha,descripcion,total,responsable) values ('$fecha','$descripcion','$total','$responsable')");
        $error=2;
      }
      
    }

    if(isset($_GET['del']))
    {        
      $codigo=$_GET['codigo'];

       $_DELETE_SQL =  "DELETE FROM $tabla_db6 WHERE id = '$codigo'";
        mysqli_query($conexion,$_DELETE_SQL); 
      $error=3;
    }
  ?>
<div class="container">
 <input type="hidden" name="agregar_producto" id="existe" value="1">
  <input type="hidden" name="no_mesa" id="mesa" value="<?php echo $mesa; ?>">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1>ADMINISTRAR EGRESOS</h1>
        <p class="lead"><strong>AGREGAR O ELIMINAR</strong></p>
        <hr>
      </div>
    </div>
    <h3><center><strong>
        <?php
        echo '<p class="bg-danger">';
           if($error=="1"){echo "ERROR, VERIFICA LOS DATOS INGRESADOS";}
           //if($error=="2"){echo "ERROR, EL USUARIO YA EXISTE (DOCUMENTO)";} 
        echo '</p>';
        echo '<p class="bg-success">';
          if($error=="2"){echo "EGRESO GUARDADO CON EXITO";}
          if($error=="3"){echo "EGRESO ELIMINADO CON EXITO";}
        echo '</p>';        
        ?>
    </strong></center></h3>
    <div class="row">         
      <div class="col-md-4"></div>
      <div class="col-md-4">         
        <div class="well">
           <form method="POST" action="administrar_egresos.php" name="egresos">
            <div class="form-group">
              <label for="producto">FECHA</label>
              <input type="text" class="form-control datepicker" id="fecha" name="fecha" value="<?php echo $fecha_inicial; ?>">
            </div>
            <div class="form-group">
              <label for="precio">DESCRIPCIÓN</label>
              <input type="text" class="form-control" placeholder="Descripción Sencilla" name="descripcion">
            </div>
            <div class="form-group">
              <label for="Enlace">TOTAL EGRESO</label>
              <input type="text" class="form-control" placeholder="Numero sin puntos" name="egreso">
            </div>
            <hr>
            <center>
              <button type="submit" class="btn btn-success btn-lg" name="guardar_egreso">GUARDAR</button>
              <a href="home.php" class="btn btn-warning btn-lg" role="button">VOLVER</a>
            </center>
          </form>
        </div>
      </div>        
      <div class="col-md-4"></div>
    </div>

  <div class="row">         
    <div class="col-md-2"></div>
    <div class="col-md-8">
    <h2><center>ULTIMOS 10 REGISTROS</center></h2>
      <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" width="100%">
          <tr>
            <th width="20%"><center>Fecha</center></th>
            <th width="50%"><center>Descripción</center></th>
            <th width="20%"><center>Total</center></th>
            <th width="10%"><center>¿Eliminar?</center></th>
          </tr>

          <?php
            $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db6 ORDER BY id DESC limit 10");
            while($consulta = mysqli_fetch_array($resultados))
            {
              echo '
                <tr>
                  <td><center>'.$consulta['fecha'].'</center></td>
                  <td><center>'.$consulta['descripcion'].'</center></td>
                  <td><center>'.number_format($consulta['total'], 0, ",", ".").'</center></td>
                  <td><center><a href="administrar_egresos.php?del=1&codigo='.$consulta['id'].'"><img src="img/x.png" width="20" height="20"></a></center></td>
                </tr>';
            }
          ?>
        </table>
      </div>
    </div>
    <div class="col-md-2"></div>
  </div>
</div><!--Containder-->
<?php include("cerrar_conexion.php"); ?>
</body>
  
</html>
