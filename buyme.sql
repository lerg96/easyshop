
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-09-2016 a las 22:14:01
-- Versión del servidor: 10.0.20-MariaDB-wsrep
-- Versión de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `u378879709_buyme`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `egreso`
--

CREATE TABLE IF NOT EXISTS `egreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `descripcion` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `total` int(11) NOT NULL,
  `responsable` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE IF NOT EXISTS `facturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `total` int(11) NOT NULL,
  `impuesto` int(11) NOT NULL,
  `responsable` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `mesa` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`id`, `fecha`, `total`, `impuesto`, `responsable`, `mesa`) VALUES
(1, '2016-09-21', 30000, 2400, 'Dostin Hurtado', '1'),
(2, '2016-09-21', 22500, 1800, 'Dostin Hurtado', '1'),
(3, '2016-09-21', 16000, 1280, 'Dostin Hurtado', '1'),
(4, '2016-09-21', 16000, 1280, 'Dostin Hurtado', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imprimir_factura`
--

CREATE TABLE IF NOT EXISTS `imprimir_factura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `size_titulo` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `nit` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `nit_activar` tinyint(1) NOT NULL DEFAULT '1',
  `direccion` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `direccion_activar` tinyint(1) NOT NULL DEFAULT '1',
  `telefono` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `telefono_activar` tinyint(1) NOT NULL DEFAULT '1',
  `tipo` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `tipo_desactivar` tinyint(1) NOT NULL DEFAULT '1',
  `linea1_activar` tinyint(1) NOT NULL DEFAULT '1',
  `tipo_tabla` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje_propina` text COLLATE utf8_unicode_ci NOT NULL,
  `mensaje_legal` text COLLATE utf8_unicode_ci NOT NULL,
  `linea2_activar` tinyint(1) NOT NULL DEFAULT '1',
  `size_general` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '3',
  `ancho_papel` int(11) NOT NULL DEFAULT '256',
  `nombre_impuesto` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `impuesto` int(11) NOT NULL,
  `impuesto_activar` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `imprimir_factura`
--

INSERT INTO `imprimir_factura` (`id`, `titulo`, `size_titulo`, `nit`, `nit_activar`, `direccion`, `direccion_activar`, `telefono`, `telefono_activar`, `tipo`, `tipo_desactivar`, `linea1_activar`, `tipo_tabla`, `mensaje_propina`, `mensaje_legal`, `linea2_activar`, `size_general`, `ancho_papel`, `nombre_impuesto`, `impuesto`, `impuesto_activar`) VALUES
(1, 'BUYME', '8', '1.121.123.456-3', 1, 'Crr 34 #3X-9X Barzal Bajo', 1, '3191236549 - 6620203', 1, 'REGIMEN SIMPLIFICADO', 1, 1, 'class="table table-condensed"', '<strong>NO INCLUYE PROPINA</strong><br>El servicio es voluntario', '<p class="text-justify">Esta Factura se asimila a una letra de cambio para todos los efectos legales, artículo No. 774 del Código de Comercio. </p>', 1, '3', 256, 'IMPC', 8, 1),
(2, '¡Y AJA!', '8', '1.121.123.456-3', 1, 'Crr 34 #3X-9X Barzal Bajo', 1, '3191236549 - 6620203', 1, 'REGIMEN SIMPLIFICADO', 1, 1, 'class="table table-condensed"', '<strong>NO INCLUYE PROPINA</strong><br>El servicio es voluntario', '<p class="text-justify">Esta Factura se asimila a una letra de cambio para todos los efectos legales, artículo No. 774 del Código de Comercio. </p>', 1, '3', 256, 'IMPC', 8, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `precio` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`codigo`, `nombre`, `link`, `precio`) VALUES
(1, 'Currambera Grande', 'productos/1.jpg', 14000),
(2, 'Currambera Mediana', 'productos/2.jpg', 8000),
(3, 'Hamburguesa', 'productos/3.jpg', 6000),
(4, 'Hamburguesa Especial', 'productos/4.jpg', 8500),
(5, 'Perro Caliente', 'productos/5.jpg', 5500),
(6, 'Patacon Mixto', 'productos/6.jpg', 10000),
(7, 'Pollo a la Plancha', 'productos/7.jpg', 8000),
(8, 'Carne a la Plancha', 'productos/8.jpg', 9000),
(9, 'Gaseosa 350ml', 'productos/9.jpg', 1500),
(10, 'Cerveza', 'productos/10.jpg', 2000),
(11, 'gaseosa 1.7lt', 'productos/11.jpg', 4000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_despachados`
--

CREATE TABLE IF NOT EXISTS `productos_despachados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mesa` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `precio` int(11) NOT NULL,
  `precio_total` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_vendidos`
--

CREATE TABLE IF NOT EXISTS `productos_vendidos` (
  `id_factura` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `mesa` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `cantidad` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `precio_unitario` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `precio_total` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `productos_vendidos`
--

INSERT INTO `productos_vendidos` (`id_factura`, `mesa`, `codigo`, `nombre`, `cantidad`, `precio_unitario`, `precio_total`) VALUES
('1', '1', '3', 'Hamburguesa', '1', '6000', '6000'),
('1', '1', '6', 'Patacon Mixto', '1', '10000', '10000'),
('1', '1', '10', 'Cerveza', '1', '2000', '2000'),
('1', '1', '9', 'Gaseosa 350ml', '1', '1500', '1500'),
('1', '1', '5', 'Perro Caliente', '1', '5500', '5500'),
('1', '1', 'x', 'Adicional de Carne', '1', '5000', '5000'),
('2', '1', '4', 'Hamburguesa Especial', '1', '8500', '8500'),
('2', '1', '7', 'Pollo a la Plancha', '1', '8000', '8000'),
('2', '1', '10', 'Cerveza', '1', '2000', '2000'),
('2', '1', '9', 'Gaseosa 350ml', '1', '1500', '1500'),
('2', '1', 'x', 'Adicional de Huevos', '1', '2500', '2500'),
('3', '1', '1', 'Currambera Grande', '1', '14000', '14000'),
('3', '1', '10', 'Cerveza', '1', '2000', '2000'),
('4', '1', '10', 'Cerveza', '1', '2000', '2000'),
('4', '1', '1', 'Currambera Grande', '1', '14000', '14000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `documento` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `rol` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`documento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`documento`, `nombre`, `telefono`, `pass`, `rol`) VALUES
('111111', 'Sandy Olivera', '111111', '111111', 'U'),
('12345', 'Dostin Hurtado', '12345', '12345', 'A');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
