<?php
	session_start();
	ob_start(); 

    //Si no inicia sesion. ¡Chao papá!
    if($_SESSION['sesion_exito']<>1){header('Location:index.php');} //Si NO inicio sesion, ¡hasta luego!
    if($_SESSION['tipo_usuario']<>"A"){header('Location:index.php');}//Si NO es administrador, Chao mijo!
	
	//abro la conexion para consultar en la base de datos
	include('../abrir_conexion.php');

	//recibo las fechas por GET con el formato SQL (y-m-d)
	$fecha_inicial=$_GET["fecha_inicial"];					
    $fecha_final=$_GET["fecha_final"];
    $tipo=$_GET["tipo"];

    //creo los archivos que exportare
	$archivo = 'informe.txt';

    if($tipo=="I")
    {
    	//abro el archivo y escribo en el los titulos del informe
		$fp=fopen($archivo,"w");
		fwrite($fp,'ID;FECHA;RESPONSABLE;TOTAL');
	    fclose($fp) ;
	    
	    $fp=fopen($archivo,"a");

		//realizo la consulta
		$resultados = mysqli_query($conexion,"SELECT * from $tabla_db5 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
		
		while($consulta = mysqli_fetch_array($resultados))
	    {
		    //Creo la linea
		    $linea = ''.$consulta['id'].';'.$consulta['fecha'].';'.$consulta['responsable'].';'.$consulta['total']."\n\r";
		    
		    //guardo la linea
		    fputs($fp,chr(13).chr(10));
		   	fwrite($fp, $linea);
		}
	}
	if($tipo=="E")
    {
		//abro el archivo y escribo en el los titulos del informe
		$fp=fopen($archivo,"w");
		fwrite($fp,'ID;FECHA;DESCRIPCION;RESPONSABLE;TOTAL');
	    fclose($fp) ;
	    
	    $fp=fopen($archivo,"a");

		//realizo la consulta
		$resultados = mysqli_query($conexion,"SELECT * from $tabla_db6 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
		
		while($consulta = mysqli_fetch_array($resultados))
	    {
		    //Creo la linea
		    $linea = ''.$consulta['id'].';'.$consulta['fecha'].';'.$consulta['descripcion'].';'.$consulta['responsable'].';'.$consulta['total']."\n\r";
		    
		    //guardo la linea
		    fputs($fp,chr(13).chr(10));
		   	fwrite($fp, $linea);
	}
	}

	//Cierro el archivo ya que guarde todas las lineas
	fclose($fp) ;									

	//cierro la conexion a la base de datos
	include('../cerrar_conexion.php');

	//$url="informe_ventas.txt";

	header ("Content-Disposition: attachment; filename=informe.txt" ); 
	header ("Content-Type: application/force-download"); 
	readfile($archivo); 
	exit; 

?>