<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Diseño y Publicidad" content="">
    <meta name="Central de Diseño" content="">
    <link rel="icon" href="img/logo.ico">
    <title>Delicias Burger</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- Bootstrap core CSS -->
    <!--<link href="./css/navbar-fixed-top.css" rel="stylesheet">-->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    

  </head>
<body>
  <?php
    session_start();
    ob_start();
    include("abrir_conexion.php"); 

    if($_SESSION['sesion_exito']<>1){header('Location:index.php');} //Si NO inicio sesion, ¡hasta luego!
    if($_SESSION['tipo_usuario']<>"A"){header('Location:index.php');}//Si NO es administrador, Chao mijo!
    $error=0;
    $accion=0;
    $id_parametro_factura=2;

    if(isset($_POST['formato_factura'])){$accion=1;}//1=Guardar cambios personalizados
    if(isset($_POST['valores_default'])){$accion=2;}//2=Restablecer todos los valores a fabrica

    if($accion==1)
    {
      //recibo los datos del formulario
      $titulo =           $_POST['titulo'];
      $size_titulo =      $_POST['size_titulo'];
      $nit =              $_POST['nit'];
      $nit_activar =      $_POST['nit_activar'];
      $direccion =        $_POST['direccion'];
      $direccion_activar =$_POST['direccion_activar'];
      $telefono =         $_POST['telefono'];
      $telefono_activar = $_POST['telefono_activar'];
      $tipo =             $_POST['tipo'];
      $tipo_desactivar =  $_POST['tipo_desactivar'];
      $linea1_activar =   $_POST['linea1_activar'];
      $tipo_tabla =       $_POST['tipo_tabla'];
      $mensaje_propina =  $_POST['mensaje_propina'];
      $mensaje_legal =    $_POST['mensaje_legal'];
      $linea2_activar =   $_POST['linea2_activar'];
      $size_general =     $_POST['size_general'];
      $ancho_papel =      $_POST['ancho_papel'];

      $impuesto =         $_POST['impuesto'];
      $nombre_impuesto =  $_POST['nombre_impuesto'];
      $impuesto_activar = $_POST['impuesto_activar'];

      //alisto los datos para actualizar el campo personalizado
      if($titulo==""){$error=2;}
      if(!ctype_digit($size_titulo)){$error=3;} //verifico que sea un entero positivo

      //impuesto
      if($nombre_impuesto==""){$nombre_impuesto="Impuesto al Consumo";}
      if(!ctype_digit($impuesto)){$error=5;} //verifico que sea un entero positivo
      if($impuesto_activar=="SI"){$impuesto_activar=1;}else{$impuesto_activar=0;}

      if(isset($_POST['linea1_activar'])){$linea1_activar=1;}else{$linea1_activar=0;}
      if(isset($_POST['linea2_activar'])){$linea2_activar=1;}else{$linea2_activar=0;}


      if($nit_activar=="SI"){$nit_activar=1;}else{$nit_activar=0;}
      if($direccion_activar=="SI"){$direccion_activar=1;}else{$direccion_activar=0;}
      if($telefono_activar=="SI"){$telefono_activar=1;}else{$telefono_activar=0;}
      if($tipo_desactivar=="SI"){$tipo_desactivar=1;}else{$tipo_desactivar=0;}
      
      if($ancho_papel==""){$ancho_papel=256;}
      if($size_general==""){$size_general=3;}
      
      if($error==0)//si Error se mantiene en 0, actualizo los parametros
      {
        $_UPDATE_SQL="UPDATE $tabla_db7 Set 
        titulo='$titulo', 
        size_titulo='$size_titulo', 
        nit='$nit', 
        nit_activar='$nit_activar', 
        direccion='$direccion', 
        direccion_activar='$direccion_activar',
        telefono='$telefono', 
        telefono_activar='$telefono_activar',  
        tipo='$tipo', 
        tipo_desactivar='$tipo_desactivar', 
        linea1_activar='$linea1_activar', 
        tipo_tabla='$tipo_tabla', 
        mensaje_propina='$mensaje_propina', 
        mensaje_legal='$mensaje_legal', 
        linea2_activar='$linea2_activar', 
        size_general='$size_general', 
        ancho_papel='$ancho_papel',
        nombre_impuesto='$nombre_impuesto',
        impuesto='$impuesto',
        impuesto_activar='$impuesto_activar'

        where id='$id_parametro_factura'"; 

        mysqli_query($conexion,$_UPDATE_SQL); 
        $error=1;
      }
    }

    if($accion==2)
    {
      $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db7 WHERE id = 1");
      while($consulta = mysqli_fetch_array($resultados))
      {
        //recibo los parametros de la factura
        $titulo =           $consulta['titulo'];
        $size_titulo =      $consulta['size_titulo'];
        $nit =              $consulta['nit'];
        $nit_activar =      $consulta['nit_activar'];
        $direccion =        $consulta['direccion'];
        $direccion_activar =$consulta['direccion_activar'];
        $telefono =         $consulta['telefono'];
        $telefono_activar = $consulta['telefono_activar'];
        $tipo =             $consulta['tipo'];
        $tipo_desactivar =  $consulta['tipo_desactivar'];
        $linea1_activar =   $consulta['linea1_activar'];
        $tipo_tabla =       $consulta['tipo_tabla'];
        $mensaje_propina =  $consulta['mensaje_propina'];
        $mensaje_legal =    $consulta['mensaje_legal'];
        $linea2_activar =   $consulta['linea2_activar'];
        $size_general =     $consulta['size_general'];
        $ancho_papel =      $consulta['ancho_papel'];
        $nombre_impuesto =  $consulta['nombre_impuesto'];
        $impuesto =         $consulta['impuesto'];
        $impuesto_activar = $consulta['impuesto_activar'];
      }

      $_UPDATE_SQL="UPDATE $tabla_db7 Set 
        titulo='$titulo', 
        size_titulo='$size_titulo', 
        nit='$nit', 
        nit_activar='$nit_activar', 
        direccion='$direccion', 
        direccion_activar='$direccion_activar',
        telefono='$telefono', 
        telefono_activar='$telefono_activar',  
        tipo='$tipo', 
        tipo_desactivar='$tipo_desactivar', 
        linea1_activar='$linea1_activar', 
        tipo_tabla='$tipo_tabla', 
        mensaje_propina='$mensaje_propina', 
        mensaje_legal='$mensaje_legal', 
        linea2_activar='$linea2_activar', 
        size_general='$size_general', 
        ancho_papel='$ancho_papel',
        nombre_impuesto='$nombre_impuesto',
        impuesto='$impuesto',
        impuesto_activar='$impuesto_activar'

        where id=2"; 

        mysqli_query($conexion,$_UPDATE_SQL); 
        $error=4;

    }

    $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db7 WHERE id = $id_parametro_factura");
    while($consulta = mysqli_fetch_array($resultados))
    {
      //recibo los parametros de la factura
      $titulo =           $consulta['titulo'];
      $size_titulo =      $consulta['size_titulo'];
      $nit =              $consulta['nit'];
      $nit_activar =      $consulta['nit_activar'];
      $direccion =        $consulta['direccion'];
      $direccion_activar =$consulta['direccion_activar'];
      $telefono =         $consulta['telefono'];
      $telefono_activar = $consulta['telefono_activar'];
      $tipo =             $consulta['tipo'];
      $tipo_desactivar =  $consulta['tipo_desactivar'];
      $linea1_activar =   $consulta['linea1_activar'];
      $tipo_tabla =       $consulta['tipo_tabla'];
      $mensaje_propina =  $consulta['mensaje_propina'];
      $mensaje_legal =    $consulta['mensaje_legal'];
      $linea2_activar =   $consulta['linea2_activar'];
      $size_general =     $consulta['size_general'];
      $ancho_papel =      $consulta['ancho_papel'];
      $nombre_impuesto =  $consulta['nombre_impuesto'];
      $impuesto =         $consulta['impuesto'];
      $impuesto_activar = $consulta['impuesto_activar'];
    }

  ?>
<div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1>PARAMETROS DE IMPRESIÓN</h1>
        <p class="lead"><strong>MODIFICAR PARAMETROS</strong></p>
        <hr>
      </div>
    </div>
    <h3><center><strong>
        <?php
        echo '<p class="bg-danger">';
           if($error=="2"){echo "ERROR, EL CAMPO TITULO ES OBLIGATORIO";}
           if($error=="3"){echo "ERROR, DIGITA UN NUMERO EN EL TAMAÑO DEL TITULO";} 
           if($error=="5"){echo "ERROR, DIGITA UN NUMERO EN EL IMPUESTO (SIN %)";} 
        echo '</p>';
        echo '<p class="bg-success">';
          if($error=="1"){echo "PARAMETROS ACTUALIZADOS CON EXITO";}
          if($error=="4"){echo "TODOS LOS PARAMETROS SE RESTABLECIERON";}
        echo '</p>';        
        ?>
    </strong></center></h3>





    <div class="row">         
      <div class="col-md-3"></div>
      <div class="col-md-6">         
        <div class="well">
        <!--inicia formulario-->


          <form method="POST" action="administrar_impresion.php">
          
          <div class="row">  
            <div class="col-md-6"> 
                <div class="form-group">
                  <label for="ancho_papel">ANCHO DE HOJA (px)**</label>
                  <input type="text" class="form-control" id="ancho_papel" name="ancho_papel" value="<?php echo $ancho_papel; ?>"><h6>1mm = 3.2px Aprox.</h6>         
                </div>
            </div>
            <div class="col-md-6"> 
              <div class="form-group">     
                <label for="size_general">TAMAÑO DE TEXTO (GENERAL)**</label>
                <input type="text" class="form-control" id="size_general" name="size_general" value="<?php echo $size_general; ?>">
              </div>
            </div>
          </div>

          <hr>

          <div class="row">  
            <div class="col-md-6"> 
                <div class="form-group">
                  <label for="nombre_impuesto">NOMBRE IMPUESTO**</label>
                  <input type="text" class="form-control" id="nombre_impuesto" name="nombre_impuesto" value="<?php echo $nombre_impuesto; ?>"><h6>Ejemplo: IMPC=Impuesto al Consumo</h6>     
                </div>
            </div>
            <div class="col-md-6"> 
              <div class="form-group">     
                <label for="impuesto">% DE IMPUESTO*</label>
                <input type="text" class="form-control" id="impuesto" name="impuesto" value="<?php echo $impuesto; ?>">
                <h6>Sin simbolos ni letras Ejemplo: 8</h6>   
              </div>
            </div>
          </div>

          <div class="row">  
            <div class="col-md-3"></div>
            <div class="col-md-6"> 
              <div class="form-group">     
                <label for="impuesto_activar">ACTIVAR IMPUESTO**</label>
                <select class="form-control" name="impuesto_activar" id="impuesto_activar">
                    <option><?php if($impuesto_activar==1){echo 'SI';}else{echo 'NO';} ?></option>
                    <option>SI</option>  
                    <option>NO</option>  
                  </select>
              </div>
              <h6>Calcula impuesto en nuevas facturas</h6>   
            </div>
            <div class="col-md-3"></div>
          </div>

          <hr>


            <div class="row">  
              <div class="col-md-6"> 
                  <div class="form-group">
                    <label for="titulo">NOMBRE DE LA EMPRESA*</label>
                    <input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo $titulo; ?>">      
                  </div>
              </div>
              <div class="col-md-6"> 
                <div class="form-group">     
                  <label for="size_titulo">TAMAÑO DEL NOMBRE*</label>
                  <input type="text" class="form-control" id="size_titulo" name="size_titulo" value="<?php echo $size_titulo; ?>">
                </div>
              </div>
            </div>

          <hr>

             <div class="row">  
              <div class="col-md-6"> 
                  <div class="form-group">
                    <label for="nit">NIT DE LA EMPRESA</label>
                    <input type="text" class="form-control" id="nit" name="nit" value="<?php echo $nit; ?>">      
                  </div>
              </div>
              <div class="col-md-6"> 
                <div class="form-group">     
                  <label for="nit_activar">¿MOSTRAR NIT?</label>
                  <select class="form-control" name="nit_activar" id="nit_activar">
                    <option><?php if($nit_activar==1){echo 'SI';}else{echo 'NO';} ?></option>
                    <option>SI</option>  
                    <option>NO</option>  
                  </select>
                </div>
              </div>
            </div>

          <hr>

            <div class="row">  
              <div class="col-md-6"> 
                  <div class="form-group">
                    <label for="direccion">DIRECCION DE LA EMPRESA</label>
                    <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $direccion; ?>">      
                  </div>
              </div>
              <div class="col-md-6"> 
                <div class="form-group">     
                  <label for="direccion_activar">¿MOSTRAR DIRECCION?</label>
                  <select class="form-control" name="direccion_activar" id="direccion_activar">
                    <option><?php if($direccion_activar==1){echo 'SI';}else{echo 'NO';} ?></option>
                    <option>SI</option>
                    <option>NO</option>  
                  </select>
                </div>
              </div>
            </div>

          <hr>

            <div class="row">  
              <div class="col-md-6"> 
                  <div class="form-group">
                    <label for="telefono">TELEFONOS DE LA EMPRESA</label>
                    <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $telefono; ?>">      
                  </div>
              </div>
              <div class="col-md-6"> 
                <div class="form-group">     
                  <label for="telefono_activar">¿MOSTRAR TELEFONO?</label>
                  <select class="form-control" name="telefono_activar" id="telefono_activar">
                    <option><?php if($telefono_activar==1){echo 'SI';}else{echo 'NO';} ?></option>
                    <option>SI</option>
                    <option>NO</option>  
                  </select>
                </div>
              </div>
            </div>

          <hr>

            <div class="row">  
              <div class="col-md-6"> 
                  <div class="form-group">
                    <label for="tipo">TIPO DE EMPRESA</label>
                    <input type="text" class="form-control" id="tipo" name="tipo" value="<?php echo $tipo; ?>">      
                  </div>
              </div>
              <div class="col-md-6"> 
                <div class="form-group">     
                  <label for="tipo_desactivar">¿MOSTRAR TIPO?</label>
                  <select class="form-control" name="tipo_desactivar" id="tipo_desactivar">
                    <option><?php if($tipo_desactivar==1){echo 'SI';}else{echo 'NO';} ?></option>
                    <option>SI</option>
                    <option>NO</option>  
                  </select>
                </div>
              </div>
            </div>

          <hr>

          <div class="form-group">
            <label for="tipo_tabla">TIPO DE TABLA</label>
            <textarea class="form-control" rows="1" name="tipo_tabla" id="tipo_tabla"><?php echo $tipo_tabla; ?></textarea> 
          </div>

          <div class="form-group">
            <label for="mensaje_propina">MENSAJE DE PROPINA</label>
            <textarea class="form-control" rows="2" name="mensaje_propina" id="mensaje_propina"><?php echo $mensaje_propina; ?></textarea>      
          </div>

          <div class="form-group">
            <label for="mensaje_legal">MENSAJE DE LEGALIDAD</label>
            <textarea class="form-control" rows="2" name="mensaje_legal" id="mensaje_legal"><?php echo $mensaje_legal; ?></textarea>      
          </div>


          <hr>

            <center>
              <h4><b>MOSTRAR LINEAS DIVISORIAS</b></h4>
              <label class="checkbox-inline">
                <input type="checkbox" id="check1" value="1" name="linea1_activar" <?php if($linea1_activar==1){echo 'checked';} ?>> 
                <b>DIVISIÓN 1</b>
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" id="check2" value="2" name="linea2_activar"<?php if($linea2_activar==1){echo 'checked';} ?>>  
                <b>DIVISIÓN 2</b>
              </label>
            </center>

          <hr>
          <i>* Datos Obligatorios</i><br>
          <i>** Si se dejan en blanco tomaran valores por defecto</i>
          <hr>
            <center>
              <button type="submit" class="btn btn-success btn-lg" name="formato_factura">GUARDAR</button>
              <a href="admin.php" class="btn btn-warning btn-lg" role="button">VOLVER</a><br>
              <hr>
              <a class="btn btn-primary btn-lg" role="button" data-toggle="modal" data-target=".defecto">VALORES POR DEFECTO</a>
            </center>

            <!--En caso de que de clic sobre ANULAR VENTA-->
              <div class="modal fade defecto" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content">


                    <div class="modal-header">
                      <h4 class="modal-title" id="mySmallModalLabel">VALORES POR DEFECTO</h4>
                    </div>
                    <div class="modal-body">
                      <p> 
                        Al restablecer los valores, todos los parametros como tamaño de hoja, fuentes, entre otros, pasan a su configuración inicial.
                        <br>
                        <h3>¿Desea Continuar?</h3>
                      </p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancelar</button>
                      <button type="submit" class="btn btn-success btn-lg" name="valores_default">RESTABLECER</button>
                      </div>
                  </div>
                </div>
              </div>
            <!--TERMINA En caso de que de clic sobre ANULAR VENTA-->






          </form>
          <!--termina formulario-->
        </div>
      </div>        
      <div class="col-md-3"></div>
    </div>





</div><!--Containder-->
<?php include("cerrar_conexion.php"); ?>
</body>
 
</html>
