<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Administracion de Negocios" content="">
    <meta name="Pequeñas empresas" content="">
    <link rel="icon" href="img/potato.ico">
    <title>Delicias Burger</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- Bootstrap core CSS -->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--Calculo los totales de la hoja-->
    <script>
        var ftotal1 = 0;

        function multiplicar1() {
            cantidad = document.getElementById("cantidad1").value;
            unitario = document.getElementById("unitario1").value;
            ftotal1 = cantidad * unitario;
            document.getElementById("total1").value = ftotal1.toLocaleString();
        }
    </script>
</head>
<body>
<?php
session_start();
ob_start();
include("abrir_conexion.php");

//Si no inicia sesion. ¡Chao papá!
if ($_SESSION['sesion_exito'] <> 1) {
    header('Location:index.php');
}

//si no tengo ningun valor en la variable MESA, me devuelve a otra pagina
if (isset($_POST['no_mesa'])) {
    $mesa = $_POST['no_mesa'];
} //no_mesa es igual a Numero de Mesa
if (isset($_GET['no_mesa'])) {
    $mesa = $_GET['no_mesa'];
}

//verifico que digiten el numero de mesa
if ($mesa == "") {
    header('Location:seleccionar_mesa.php?error=1');
} else {
    //verifico que digiten un NUMERO como mesa
    if (!ctype_digit($mesa)) {
        header('Location:seleccionar_mesa.php?error=4');
    }
}

$error = "";
//boton de eliminar fue presionado
if (isset($_GET['del'])) {
    $del = 0;
    $id = 0;
    $del = $_GET['del'];
    $id = $_GET['id'];
    if ($del == 1) {
        $_DELETE_SQL = "DELETE FROM $tabla_db3 WHERE id = '$id'";
        mysqli_query($conexion, $_DELETE_SQL);
    }
}

if (isset($_POST['agregar_producto']) || isset($_GET['producto_existente'])) {
    if (isset($_POST['a1']))//añadio otro producto
    {
        if ($mesa == "") {
            header('Location:seleccionar_mesa.php?error=1');
        } else {
            $error = 0;
            $codigo = "x";
            $cantidad = $_POST['cantidad'];
            $nombre = $_POST['descripcion'];
            $precio = $_POST['vr_unitario'];
            $precio_total = $cantidad * $precio;

            if ($cantidad == "" || $nombre == "" || $precio == "") {
                $error = 1;
            } else {
                mysqli_query($conexion, "INSERT INTO $tabla_db3 (mesa,codigo,cantidad,nombre,precio,precio_total) values ('$mesa','$codigo','$cantidad','$nombre','$precio','$precio_total')");
            }
        }
    } else //añadio producto ya registrado en tabla "Productos"
    {
        if ($mesa == "") {
            header('Location:seleccionar_mesa.php?error=1');
        } else {
            $codigo = $_GET['producto_existente'];

            $cantidad = "1";
            $resultados = mysqli_query($conexion, "SELECT * FROM $tabla_db2 WHERE codigo = $codigo");
            while ($consulta = mysqli_fetch_array($resultados)) {
                $nombre = $consulta['nombre'];
                $precio = $consulta['precio'];
                $precio_total = $cantidad * $precio;
            }

            mysqli_query($conexion, "INSERT INTO $tabla_db3 (mesa,codigo,cantidad,nombre,precio,precio_total) values ('$mesa','$codigo','$cantidad','$nombre','$precio','$precio_total')");
        }
    }
}

$total_general = 0;
$ganancia_general = 0;
$resultados = mysqli_query($conexion, "SELECT * FROM $tabla_db3 WHERE mesa = $mesa");
while ($consulta = mysqli_fetch_array($resultados)) {
    $total_general = $total_general + $consulta['precio_total'];
    $ganancia_general = $ganancia_general + $consulta['ganancia'];
}
?>

<div class="container">
    <form method="POST" action="administrar_venta.php" name="nueva_venta">
        <input type="hidden" name="agregar_producto" id="existe" value="1">
        <input type="hidden" name="no_mesa" id="mesa" value="<?php echo $mesa; ?>">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>ADMINISTRAR VENTA</h1>
                <p class="lead"><strong>PEDIDO No. <?php echo $mesa; ?></strong></p>
                <hr>
            </div>
        </div>
        <h3>
            <center><strong>
                    <?php
                    echo '<p class="bg-danger">';
                    if ($error == "1") {
                        echo "ERROR, TODOS LOS CAMPOS SON OBLIGATORIOS";
                    }
                    echo '</p>';
                    ?>
                </strong></center>
        </h3>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="well">
                    <center>
                        <h1><strong>SALDO TOTAL<br><?php echo number_format($total_general, 0, ',', '.'); ?></strong>
                        </h1>
                        <h1><small>GANANCIA TOTAL <br><?php echo number_format($ganancia_general, 0, ',', '.'); ?></small></h1>
                    </center>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>


        <br>
        <hr>
        <!--AQUI-->


        <!-- HASTA AQUI-->
        <hr>
        <br>

        <!--Termina Well-->
        <!--Termina AÑADIR PRODUCTOS-->
        <a name="borrar"></a>
        <center><h2><b>LISTA DE PRODUCTOS</b></h2></center>

        <div class="well">
            <!-- TERMINA TABLA DE PRODUCTOS!-->
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" width="60%">
                            <tr>
                                <td width="5%">
                                    <center><strong>X</strong></center>
                                </td>
                                <td width="10%">
                                    <center><strong>CANTIDAD</strong></center>
                                </td>
                                <td width="50%">
                                    <center><strong>DESCRIPCIÓN</strong></center>
                                </td>
                                <td width="15%">
                                    <center><strong>VALOR UNITARIO</strong></center>
                                </td>
                                <td width="20%">
                                    <center><strong>VALOR TOTAL</strong></center>
                                </td>
                            </tr>
                            <?php
                            $total_general = 0;
                            $ganancia_general = 0;
                            $resultados = mysqli_query($conexion, "SELECT * FROM $tabla_db3 WHERE mesa = $mesa");
                            while ($consulta = mysqli_fetch_array($resultados)) {
                                echo '
                      <tr>
                        <td><center><a href="administrar_venta.php?del=1&id=' . $consulta['id'] . '&no_mesa=' . $consulta['mesa'] . '#borrar"><img src="img/x.png" width="20" height="20"></a></center></td>
                        <td><input type="text" class="form-control" value="' . $consulta['cantidad'] . '" disabled></td>
                        <td><input type="text" class="form-control" value="' . $consulta['nombre'] . '" disabled></td>
                        <td><input type="text" class="form-control" value="' . number_format($consulta['precio'], 0, ",", ".") . '" disabled></td>
                        <td><input type="text" class="form-control" value="' . number_format($consulta['precio_total'], 0, ",", ".") . '" disabled></td>
                     
                      </tr>
                      ';
                                $total_general = $total_general + $consulta['precio_total'];
                                $ganancia_general = $ganancia_general +  intval($consulta['ganancia']);
                    }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <center>
            <a href="seleccionar_pedidos.php" class="btn btn-warning btn-lg" role="button">SEGUIR VENDIENDO</a><br>
            <hr>
            <a class="btn btn-success btn-lg" role="button" data-toggle="modal" data-target=".vender">FINALIZAR
                VENTA</a>
            <a class="btn btn-danger btn-lg" role="button" data-toggle="modal" data-target=".anular">ANULAR VENTA</a>

        </center>

        <br><br>
        <!--En caso de que de clic sobre FINALIZAR VENTA-->
        <div class="modal fade vender" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">


                    <div class="modal-header">
                        <h4 class="modal-title" id="mySmallModalLabel">FINALIZAR VENTA</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            Esta finalizando la venta de la <strong>MESA <?php echo $mesa; ?></strong>, al finalizar la
                            venta <strong>NO PODRA VOLVER A EDITAR ESTA FACTURA</strong>
                            <br>
                        <h3>¿Desea Finalizarla?</h3>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancelar</button>&nbsp;
                        <a class="btn btn-success btn-lg" role="button"
                           href="finalizar_venta.php?anular=0&mesa=<?php echo $mesa ?>">FINALIZAR VENTA</a>
                    </div>
                </div>
            </div>
        </div>
        <!--TERMINA En caso de que de clic sobre FINALIZAR VENTA-->

        <!--En caso de que de clic sobre ANULAR VENTA-->
        <div class="modal fade anular" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">


                    <div class="modal-header">
                        <h4 class="modal-title" id="mySmallModalLabel">ANULAR VENTA</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            Esta anulando la venta de la <strong>MESA <?php echo $mesa; ?></strong>, despues de anularla
                            <strong>NO PODRA RECUPERAR ESTA LISTA DE PRODUCTOS</strong> y se liberara la mesa
                            <br>
                        <h3>¿Desea Anular?</h3>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancelar</button>
                        <a class="btn btn-danger btn-lg" role="button"
                           href="finalizar_venta.php?anular=1&mesa=<?php echo $mesa ?>">ANULAR VENTA</a>
                    </div>
                </div>
            </div>
        </div>
        <!--TERMINA En caso de que de clic sobre ANULAR VENTA-->

    </form>
</div><!--Containder-->
<?php include("cerrar_conexion.php"); ?>

</body>

</html>
