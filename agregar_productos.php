<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Diseño y Publicidad" content="">
    <meta name="Central de Diseño" content="">
    <link rel="icon" href="img/logo.ico">
    <title>Ventas</title>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="./css/navbar-fixed-top.css" rel="stylesheet">-->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./js/ie-emulation-modes-warning.js"></script>
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./js/ie10-viewport-bug-workaround.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--Calculo los totales de la hoja-->
</head>
<body>
<?php
session_start();
ob_start();
include("abrir_conexion.php");

//Si no inicia sesion. ¡Chao papá!
if ($_SESSION['sesion_exito'] <> 1) {
    header('Location:index.php');
} //Si NO inicio sesion, ¡hasta luego!
if ($_SESSION['tipo_usuario'] <> "A") {
    header('Location:index.php');
}//Si NO es administrador, Chao mijo!

$error = 0;

//agregar producto
if (isset($_POST['guardar_producto'])) {
    $nombre = $_POST['producto'];
    $link = $_POST['enlace'];
    $precio = $_POST['precio'];
    $precio_compra = $_POST['precio_compra'];
    $cantidad = $_POST['cantidad'];

    if ($nombre == "" || $precio == "" || $precio_compra == "" || $cantidad == "") //rectifico que los datos basicos fueron ingresados
    {
        $error = 3;
    } else {

//trabajo la imagen

        // Recibo los datos de la imagen
        $nombre_img = $_FILES['imagen']['name']; //guardo el nombre
        $tipo = $_FILES['imagen']['type'];        //guardo el tipo de archivo
        $tamano = $_FILES['imagen']['size'];      //guardo el tamaño del archivo

        //Si existe imagen y tiene un tamaño correcto
        if (($nombre_img == !NULL) && ($tamano <= 2000000)) //2MB
        {
            //indicamos los formatos que permitimos subir a nuestro servidor
            if (($_FILES["imagen"]["type"] == "image/gif")
                || ($_FILES["imagen"]["type"] == "image/jpeg")
                || ($_FILES["imagen"]["type"] == "image/jpg")
                || ($_FILES["imagen"]["type"] == "image/png")) {

                mysqli_query($conexion, "INSERT INTO $tabla_db2 (nombre,precio,precio_compra,cantidad) values ('$nombre','$precio','$precio_compra','$cantidad')");

                //extraigo el ID del producto
                $id_producto = mysqli_insert_id($conexion);


                // Ruta donde se guardarán las imágenes que subamos
                $destino = 'productos/';

                // Muevo la imagen a una carpeta temporal para evitar reescribir otras imagenes
                move_uploaded_file($_FILES['imagen']['tmp_name'], $destino . $nombre_img);

                //Divido el nombre y la extencion en 2 variables diferentes
                list($solo_nombre, $extencion) = explode(".", $nombre_img);
                //Creo el nuevo nombre que tendra la imagen
                $nuevo_nombre = "$id_producto" . "." . "$extencion";
                //renombro la imagen con el nuevo nombre
                rename("$destino/$nombre_img", "$destino/$nuevo_nombre");

                //guardo el destino con el nombre de la imagen
                $destino = $destino . $nuevo_nombre;

                //actualizo la base de datos con la direccion de la imagen
                $_UPDATE_SQL = "UPDATE $tabla_db2 Set 
                link='$destino' 
                where codigo='$id_producto'";
                mysqli_query($conexion, $_UPDATE_SQL);

                $error = 1;
            } else {
                $error = 5;
            }//error 5 Formato Invalido
        } else {
            if ($nombre_img == !NULL) {
                $error = 4;
            } //error 4 Excede el tamaño
        }
    }
}

//eliminar producto
if (isset($_GET['del'])) {
    $codigo = $_GET['codigo'];

    $resultados = mysqli_query($conexion, "SELECT * FROM $tabla_db2 WHERE codigo = '$codigo'");
    while ($consulta = mysqli_fetch_array($resultados)) {
        $destino = $consulta['link'];
    }

    unlink($destino);

    $_DELETE_SQL = "DELETE FROM $tabla_db2 WHERE codigo = '$codigo'";
    mysqli_query($conexion, $_DELETE_SQL);
    $error = 2;
}
?>
<div class="container">
    <input type="hidden" name="agregar_producto" id="existe" value="1">
    <input type="hidden" name="no_mesa" id="mesa" value="<?php echo $mesa; ?>">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>AGREGAR PRODUCTOS</h1>
            <p class="lead"><strong>Añadir, Eliminar o Modificar</strong></p>
            <hr>
        </div>
    </div>

    <h3>
        <center><strong>
                <?php
                echo '<p class="bg-danger">';
                if ($error == "3") {
                    echo "ERROR, TODOS LOS DATOS SON OBLIGATORIOS";
                }
                if ($error == "4") {
                    echo "ERROR, LA IMAGEN ES MUY GRANDE (MAXIMO 2MB)";
                }
                if ($error == "5") {
                    echo "ERROR, FORMATO DE IMAGEN NO PERMITIDO";
                }
                echo '</p>';
                echo '<p class="bg-success">';
                if ($error == "1") {
                    echo "PRODUCTO GUARDADO CON EXITO";
                }
                if ($error == "2") {
                    echo "PRODUCTO ELIMINADO CON EXITO";
                }
                echo '</p>';
                ?>
            </strong></center>
    </h3>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="well">
                <form method="POST" action="agregar_productos.php" name="nuevo_producto" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="producto">PRODUCTO</label>
                        <input type="text" class="form-control" id="producto" placeholder="Nombre Corto"
                               name="producto">
                    </div>
                    <div class="form-group">
                        <label for="precio">PRECIO DE VENTA</label>
                        <input type="text" class="form-control" id="precio" placeholder="Precio sin puntos"
                               name="precio">
                    </div>
                    <div class="form-group">
                        <label for="precio_compra">PRECIO DE COMPRA</label>
                        <input type="text" class="form-control" id="precio_compra" placeholder="Precio sin puntos"
                               name="precio_compra">
                    </div>
                    <div class="form-group">
                        <label for="cantidad">CANTIDAD</label>
                        <input type="text" class="form-control" id="cantidad" placeholder="Cantidad sin puntos"
                               name="cantidad">
                    </div>
                    <div class="form-group">
                        <label for="imagen">Agregar Foto</label>
                        <input type="file" id="imagen" name="imagen">
                        <p class="help-block">Jpg, Png, Gif, Maximo 2MB.</p>
                    </div>
                    <hr>
                    <center>
                        <button type="submit" class="btn btn-success btn-lg" name="guardar_producto">GUARDAR</button>
                        <a href="admin.php" class="btn btn-warning btn-lg" role="button">VOLVER</a>
                    </center>
                </form>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" width="100%">
                    <tr>
                        <th width="5%">
                            <center>Codigo</center>
                        </th>
                        <th width="15%">
                            <center>Producto</center>
                        </th>
                        <th width="15%">
                            <center>Precio de venta</center>
                        </th>
                        <th width="15%">
                            <center>Precio de compra</center>
                        </th>
                        <th width="10%">
                            <center>Ganancia</center>
                        </th>
                        <th width="5%">
                            <center>Cantidad</center>
                        </th>
                        <th width="5%">
                            <center>¿Acción?</center>
                        </th>
                    </tr>

                    <?php
                    $resultados = mysqli_query($conexion, "SELECT * FROM $tabla_db2");
                    while ($consulta = mysqli_fetch_array($resultados)) {
                        $ganancia = (intval($consulta['precio']) - intval($consulta['precio_compra']));
                        echo '
                <tr>
                  <td><center>' . $consulta['codigo'] . '</center></td>
                  <td><center>' . $consulta['nombre'] . '</center></td>
                  <td><center>' . $consulta['precio'] . '</center></td>
                  <td><center>' . $consulta['precio_compra'] . '</center></td>
                  <td><center>' . $ganancia . '</center></td>
                  <td><center>' . $consulta['cantidad'] . '</center></td>
                  <td><center><a href="agregar_productos.php?del=1&codigo=' . $consulta['codigo'] . '"><img src="img/x.png" width="20" height="20"></a>
                   | <a href="editar_producto.php?codigo=' . $consulta['codigo'] . '"><img src="img/pedido.png" width="20" height="20"></a></center></td>
                </tr>';
                    }
                    ?>
                </table>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div><!--Containder-->
<?php include("cerrar_conexion.php"); ?>
</body>

</html>
