<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Administracion de Negocios" content="">
    <meta name="Pequeñas y Grandes empresas" content="">
    <link rel="icon" href="img/favicon.ico">
    <title>Buyme - Donativo</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
<body>
  <?php
    session_start();
    ob_start();

    if($_SESSION['sesion_exito']<>1){header('Location:index.php');} //Si NO inicio sesion, ¡hasta luego!
    if(isset($_GET['tipo'])){$tipo=$_GET['tipo'];}

    if($tipo==1)
    {
    echo'
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h1>REALIZAR DONACIÓN</h1>
            <p class="lead"><strong>Metodo PayPal</strong></p>
            <hr>
          </div>
        </div>
         <div class="row">         
          <div class="col-md-3"></div>
          <div class="col-md-6">         
            <div class="well">
              <center>
                <h3><strong>¡GRACIAS POR FINANCIAR ESTE PROYECTO!</strong></h3>
              </center>
              <center><img src="img/paypal.png" alt="Tarjeta de Credito" class="img-circle" width="150"></center><br>
              <p align="justify">
                A continuacion, sientase libre de elegir el monto a donar, despues procede a dar clic en <b>"Donate"</b>, y sigue los pasos en la <b>pagina oficial de Paypal</b> donde le pediran (si no tiene cuenta) sus datos personales.
              </p>
              <p><i>En <b>Paypal</b> podra hacer uso de cualquier tarjeta de credito (Visa, MasterCard, DinnerClub, etc...) y algunas Debito.</i></p>
              <br>
              <p><strong>Por favor, elige el monto que deseas Donar.</strong></p>
              <center>
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                  <input name="cmd" type="hidden" value="_s-xclick" /> 
                  <input name="hosted_button_id" type="hidden" value="YRKTLZSAJ97QC" />
                  <table>
                    <tbody>
                      <tr>
                        <td><input name="on0" type="hidden" value="Donativo"/></td>
                      </tr>
                      <tr>
                        <td>
                          <select name="os0" class="form-control">
                            <option value="Donar 5:">Donar: $5 USD</option>
                            <option value="Donar 10:">Donar: $10 USD</option>
                            <option value="Donar 20:">Donar: $20 USD</option>
                            <option value="Donar 50:">Donar: $50 USD</option>
                            <option value="Donar 100:">Donar: $100 USD</option>
                            <option value="Donar 500:">Donar: $500 USD</option>
                            <option value="Donar 1000:">Donar: $1,000 USD</option>
                          </select>
                        </td>
                      </tr>
                    </tbody>
                  </table><br>
                  <input name="currency_code" type="hidden" value="USD" /> 
                  <input alt="PayPal - The safer, easier way to pay online!" name="submit" src="img/btn_donateCC_LG.gif" type="image" /> 
                  <img src="img/pixel.gif" alt="" width="1" height="1" border="0" />
                </form>
              </center>         
              <hr />
              <p style="text-align: center;"><strong> SI TIENES ALGUNA DUDA NO DUDES EN CONTACTARME</strong></p>
              <center>
                <a class="btn btn-primary" href="http://www.dostinhurtado.com/main/contacto" target="_blank">CONTACTAME</a>  
              </center>
            </div>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    ';
  }

   if($tipo==2)
    {
    echo'
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h1>REALIZAR DONACIÓN</h1>
            <p class="lead"><strong>Transferencia Internacional</strong></p>
            <hr>
          </div>
        </div>
         <div class="row">         
          <div class="col-md-3"></div>
          <div class="col-md-6">         
            <div class="well">
              <center>
                <h3><strong>¡AYUDA A FINANCIAR ESTE PROYECTO!</strong></h3>
              </center>
              <center><img src="img/western.png" alt="Tarjeta de Credito" class="img-circle" width="150"></center><br>
              <p align="justify">
                <a href="https://www.westernunion.com/" target="_blank">Western Unión</a>, 
                <a href="https://www.xoom.com/" target="_blank">Xoom</a>,
                <a href="https://secure.moneygram.com/" target="_blank">MoneyGram</a>,  (O la entidad que prefieras) tienen 2 metodos de funcionamiento, el mas facil seria acceder al sitio web oficial e intentar realizar el metodo online (como Xoom) y el otro seria acercarse a una oficina (Como Western Union) y realizar el donativo por ventanilla.
              </p>
              <p>
              Independientemente de como quieras realizar el donativo, vas a necesitar unos datos personales:
              </p>

              <center>
                <table class="table table-hover">
                  <tr class="success">
                    <td colspan="2"><center><b>DATOS PERSONALES</b></center></td>                    
                  </tr>
                  <tr>
                    <td><b>Nombre completo:</b></td>
                    <td>Oscar Dustin Hurtado Pico</td>
                  </tr>
                   <tr>
                    <td><b>Documento de Identidad (CC):</b></td>
                    <td>1.121.885.849</td>
                  </tr>
                   <tr>
                    <td><b>Pais:</b></td>
                    <td>Colombia</td>
                  </tr>
                   <tr>
                    <td><b>Ciudad:</b></td>
                    <td>Villavicencio</td>
                  </tr>
                   <tr>
                    <td><b>Codigo Postal:</b></td>
                    <td>500005</td>
                  </tr>
                  <tr>
                    <td><b>Correo Electronico:</b></td>
                    <td>Contacto@DostinHurtado.com</td>
                  </tr>
                  <tr class="success">
                    <td colspan="2"><center><b>CUENTA BANCARIA (OPCIONAL)</b></center></td>                    
                  </tr>
                  <tr>
                    <td><b>Banco:</b></td>
                    <td>Bancolombia</td>
                  </tr>
                   <tr>
                    <td><b>Numero de Cuenta:</b></td>
                    <td>841-419436-76</td>
                  </tr>
                  <tr>
                    <td><b>Tipo de Cuenta:</b></td>
                    <td>Ahorros</td>
                  </tr>
                </table>
              </center>
            <p align="center"><b>TENGA EN CUENTA</b> que al realizar el donativo le darán un código de aprobación necesario para retirar el donativo.</p>
              <hr />
              <p style="text-align: center;"><strong> ¿DUDAS?, ¡NO DUDES EN CONTACTARME!</strong></p>
              <center>
                <a class="btn btn-primary" href="http://www.dostinhurtado.com/main/contacto" target="_blank">CONTACTAME</a>  
              </center>
            </div>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    ';
  }

  if($tipo==3)
    {
    echo'
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h1>REALIZAR DONACIÓN</h1>
            <p class="lead"><strong>Transferencia Nacional</strong></p>
            <hr>
          </div>
        </div>
         <div class="row">         
          <div class="col-md-3"></div>
          <div class="col-md-6">         
            <div class="well">
              <center>
                <h3><strong>¡AYUDA A FINANCIAR ESTE PROYECTO!</strong></h3>
              </center>
              <center><img src="img/efecty.png" alt="Tarjeta de Credito" class="img-circle" width="150"></center><br>
              <p align="justify">
                <b>¡Hola Compadre, que gusto saludar a otro Colombiano!</b>.
              </p>
              <p>
                Como sabras, realizar un Giro aqui <b>¡es muy facil!</b>, solo acercate a un <b>Efecty, SuperGiros, MovilRed, Baloto, Giros y Finanzas, Dimonex, Servientrega, ConSuerte o Bancos como: Bancolombia, Davivienda, BBVA...</b> En fin, la lista sigue y sigue y por cualquiera de estas entidades puedes realizar tu donativo.
              </p>
              <p>
              Independientemente de como quieras realizar el donativo, vas a necesitar unos datos personales:
              </p>

              <center>
                <table class="table table-hover">
                  <tr class="success">
                    <td colspan="2"><center><b>DATOS PERSONALES</b></center></td>                    
                  </tr>
                  <tr>
                    <td><b>Nombre completo:</b></td>
                    <td>Oscar Dustin Hurtado Pico</td>
                  </tr>
                   <tr>
                    <td><b>Documento de Identidad (CC):</b></td>
                    <td>1.121.885.849</td>
                  </tr>
                   <tr>
                    <td><b>Departamento:</b></td>
                    <td>Meta</td>
                  </tr>
                   <tr>
                    <td><b>Ciudad:</b></td>
                    <td>Villavicencio</td>
                  </tr>
                   <tr>
                    <td><b>Codigo Postal:</b></td>
                    <td>500005</td>
                  </tr>
                  <tr>
                    <td><b>Correo Electronico:</b></td>
                    <td>Contacto@DostinHurtado.com</td>
                  </tr>
                   <tr>
                    <td><b>Cel - Whatsapp:</b></td>
                    <td>316 627 2815</td>
                  </tr>
                  <tr class="success">
                    <td colspan="2"><center><b>CUENTA BANCARIA (OPCIONAL)</b></center></td>                    
                  </tr>
                  <tr>
                    <td><b>Banco:</b></td>
                    <td>Bancolombia</td>
                  </tr>
                   <tr>
                    <td><b>Numero de Cuenta:</b></td>
                    <td>841-419436-76</td>
                  </tr>
                  <tr>
                    <td><b>Tipo de Cuenta:</b></td>
                    <td>Ahorros</td>
                  </tr>
                </table>
              </center>
           <p align="center"><b>TENGA EN CUENTA</b> que al realizar el donativo le darán un código de aprobación necesario para retirar el donativo.</p>
              <hr />
              <p>
                <center>
                  <h3><b>¿DUDAS?, ¡COMO SE LE OCURRE!</b></h3>Regaleme una llamada al <b>316 627 2815</b> y con gusto despejo tus dudas.
                </center>
              </p>
              <center>
                <a class="btn btn-primary" href="http://www.dostinhurtado.com/main/contacto" target="_blank">CONTACTAME</a>  
              </center>
            </div>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    ';
  }

  ?>
<center><a class="btn btn-warning btn-lg" href="acerca.php" role="button">VOLVER A ACERCA DE...</a></center><br><br>
</body>
  <?php include("creador.php"); ?>
</html>