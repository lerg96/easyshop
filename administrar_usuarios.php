<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Diseño y Publicidad" content="">
    <meta name="Central de Diseño" content="">
    <link rel="icon" href="img/logo.ico">
    <title>Ventas</title>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="./css/navbar-fixed-top.css" rel="stylesheet">-->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./js/ie-emulation-modes-warning.js"></script>
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./js/ie10-viewport-bug-workaround.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--Calculo los totales de la hoja-->
  </head>
<body>
  <?php
    session_start();
    ob_start();
    include("abrir_conexion.php"); 

    //Si no inicia sesion. ¡Chao papá!
    if($_SESSION['sesion_exito']<>1){header('Location:index.php');} //Si NO inicio sesion, ¡hasta luego!
    if($_SESSION['tipo_usuario']<>"A"){header('Location:index.php');}//Si NO es administrador, Chao mijo!

    if(isset($_GET['accion'])){$accion=$_GET['accion'];}

    if(isset($_POST['guardar_usuario']))
    {
      $documento=$_POST['documento'];
      $nombre=$_POST['nombre'];
      $telefono=$_POST['telefono'];
      $pass=$_POST['pass'];
      $rol=$_POST['rol'];
      if($rol=="Administrador")
      {$rol="A";}
      else
      {$rol="U";}

      if($documento<>"" && $rol<>"" && $pass<>"" && $nombre<>"" && $telefono<>"")
      {
        $existe=0;
         $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db1 WHERE documento = '$documento'");
          while($consulta = mysqli_fetch_array($resultados))
          {$existe=1;}
        
        if($existe==1){$error=2;}
        else
        {
          mysqli_query($conexion, "INSERT INTO $tabla_db1 (documento,nombre,telefono,pass,rol) values ('$documento','$nombre','$telefono','$pass','$rol')"); 
          $error=0;//EXITO TOTAL 
        }
      }
      else
      {$error=1;} 
    }

    if($accion==1)
    {       
      $documento=$_GET['documento'];

       $_DELETE_SQL =  "DELETE FROM $tabla_db1 WHERE documento = '$documento'";
        mysqli_query($conexion,$_DELETE_SQL); 
        $error=3;
    }
  ?>
<div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1>ADMINISTRAR USUARIOS</h1>
        <p class="lead"><strong>Añadir, Eliminar o Modificar</strong></p>
        <hr>
      </div>
    </div>
    <h3><center><strong>
        <?php
        echo '<p class="bg-danger">';
           if($error=="1"){echo "ERROR, TODOS LOS CAMPOS SON OBLIGATORIOS";}
           if($error=="2"){echo "ERROR, EL USUARIO YA EXISTE (DOCUMENTO)";} 
        echo '</p>';
        echo '<p class="bg-success">';
          if($error=="0"){echo "USUARIO GUARDADO CON EXITO";}
          if($error=="3"){echo "USUARIO ELIMINADO CON EXITO";}
        echo '</p>';        
        ?>
    </strong></center></h3>
        
    <div class="row">         
      <div class="col-md-4"></div>
      <div class="col-md-4">         
        <div class="well">
           <form method="POST" action="administrar_usuarios.php" name="form_usuario">
            <div class="form-group">
              <label for="documento">DOCUMENTO*</label>
              <input type="text" class="form-control" id="documento" placeholder="Documento de Identidad" name="documento">
            </div>
            <div class="form-group">
              <label for="nombre">NOMBRE*</label>
              <input type="text" class="form-control" id="nombre" placeholder="1 Nombre y 1 Apellido" name="nombre">
            </div>
            <div class="form-group">
              <label for="telefono">TELEFONO*</label>
              <input type="text" class="form-control" id="telefono" placeholder="Celular" name="telefono">
            </div>
            <div class="form-group">
              <label for="pass">CONTRASEÑA*</label>
              <input type="password" class="form-control" id="pass" placeholder="Contraseña" name="pass">
            </div>
            <div class="form-group">
             <label for="precio">TIPO DE USUARIO*</label>
              <select class="form-control" name="rol">
                <option>Usuario</option>
                <option>Administrador</option>
              </select>
            </div>
            <hr>
            <center>
              <button type="submit" class="btn btn-success btn-lg" name="guardar_usuario">GUARDAR</button>
              <a href="admin.php" class="btn btn-warning btn-lg" role="button">CANCELAR</a>
            </center>
          </form>
        </div>
      </div>        
      <div class="col-md-4"></div>
    </div>

  <div class="row">         
    <div class="col-md-1"></div>
    <div class="col-md-10">
      <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" width="100%">
          <tr>
            <th width="10%"><center>Documento</center></th>
            <th width="40%"><center>Nombre</center></th>
            <th width="20%"><center>Telefono</center></th>
            <th width="20%"><center>Contraseña</center></th>
            <th width="10%"><center>Rol</center></th>
            <th width="10%"><center>¿Borrar?</center></th>
          </tr>

          <?php
            $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db1");
            while($consulta = mysqli_fetch_array($resultados))
            {
              echo '
                <tr>
                  <td><center>'.$consulta['documento'].'</center></td>
                  <td><center>'.$consulta['nombre'].'</center></td>
                  <td><center>'.$consulta['telefono'].'</center></td>
                  <td><center>'.$consulta['pass'].'</center></td>
                  <td><center>'.$consulta['rol'].'</center></td>
                  <td><center><a href="administrar_usuarios.php?accion=1&documento='.$consulta['documento'].'"><img src="img/x.png" width="20" height="20"></a></center></td>
                </tr>';
            }
          ?>
        </table>
      </div>
    </div>
    <div class="col-md-1"></div>
  </div>
</div><!--Containder-->
<?php include("cerrar_conexion.php"); ?>
</body>
  
</html>
