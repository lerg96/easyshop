<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Diseño y Publicidad" content="">
    <meta name="Central de Diseño" content="">
    <link rel="icon" href="img/logo.ico">
    <title>Administracion</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- Bootstrap core CSS -->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--JavaScrip para las FECHAS-->
  <script>
      $(function(){
        $('.datepicker').datepicker();
      });
    </script>

  </head>
<body>
  <?php
    session_start();
    ob_start();

    if($_SESSION['sesion_exito']<>1){header('Location:index.php');} //Si NO inicio sesion, ¡hasta luego!
    if($_SESSION['tipo_usuario']<>"A"){header('Location:index.php');}//Si NO es administrador, Chao mijo!
  ?>

<div class="container">

		<div class="row">
			<div class="col-lg-12 text-center">
				<h1>CENTRO DE MANDO</h1>
				<p class="lead">SOLO ADMINISTRADORES</p>
				<hr>
			</div>
		</div>
		<br>
		<div class="row">
      <div class="col-md-4">
     		<div class="well">
        	<center>
          	<h2>INFORMES DE INGRESOS</h2>
	        	<p><img src="img/ingreso.jpg" alt="Informes" class="img-circle" width="200" height="200"></p>
       		
	        	<div class="row">
	        		<div class="col-md-2"></div>
	        		<div class="col-md-8">

			        	<form method="POST" action="generar_informe.php" name="informe_ingresos">
			            <div class="form-group">
			              <label for="fecha_inicial">FECHA INICIAL</label><br>
			              <input type="text" class="form-control datepicker" id="fecha_inicial" name="fecha_inicial" placeholder="Fecha Desde...">
			            </div>
			            <div class="form-group">
			              <label for="fecha_final">FECHA FINAL</label>
			              <input type="text" class="form-control datepicker" id="fecha_final" name="fecha_final" placeholder="Fecha Hasta...">
			            </div>
			            <p><button type="submit" class="btn btn-success" name="informe_ingreso">GENERAR INFORME</button></p>
		            </form>

           	 	</div>
           	 	<div class="col-md-2"></div>
           	</div>
          </center>
       	</div>
      </div>
      <div class="col-md-4">
      	<div class="well">
        	<center>
          	<h2>INFORMES DE EGRESOS</h2>
	        	<p><img src="img/egreso.jpg" alt="Informes" class="img-circle" width="200" height="200"></p>

	        	<div class="row">
	        		<div class="col-md-2"></div>
	        		<div class="col-md-8">

			        	<form method="POST" action="generar_informe.php" name="informe_egresos">
			            <div class="form-group">
			              <label for="fecha_inicial">FECHA INICIAL</label><br>
			              <input type="text" class="form-control datepicker" id="fecha_inicial" name="fecha_inicial" placeholder="Fecha Desde...">
			            </div>
			            <div class="form-group">
			              <label for="fecha_final">FECHA FINAL</label>
			              <input type="text" class="form-control datepicker" id="fecha_final" name="fecha_final" placeholder="Fecha Hasta...">
			            </div>
			            <p><button type="submit" class="btn btn-danger" name="informe_egreso">GENERAR INFORME</button></p>
		            </form>

           	 	</div>
           	</div>
       		</center>
       	</div>
      </div> 

       <div class="col-md-4">
        <div class="well">
          <center>
            <h2>PRODUCTOS VENDIDOS</h2>
            <p><img src="img/ventas.jpg" alt="Informes" class="img-circle" width="200" height="200"></p>
          
            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-8">

                <form method="POST" action="generar_informe_productos.php">
                  <div class="form-group">
                    <label for="fecha_inicial">FECHA INICIAL</label><br>
                    <input type="text" class="form-control datepicker" id="fecha_inicial" name="fecha_inicial" placeholder="Fecha Desde...">
                  </div>
                  <div class="form-group">
                    <label for="fecha_final">FECHA FINAL</label>
                    <input type="text" class="form-control datepicker" id="fecha_final" name="fecha_final" placeholder="Fecha Hasta...">
                  </div>
                  <p><button type="submit" class="btn btn-success" name="informe_productos" name="productos">GENERAR INFORME</button></p>
                </form>

              </div>
            </div>
          </center>
        </div>
      </div>

      
    </div>
    <br><hr><br>
    <div class="row">
      <div class="col-md-4">
        <div class="well">
		     	<center>
	          	<h2>LISTA DE PRODUCTOS</h2>
	          	<p><a href="agregar_productos.php"><img src="img/productos.jpg" class="img-circle" width="200" height="200"></a></p>
	          	<p><a class="btn btn-primary" href="agregar_productos.php" role="button">AGREGAR PRODUCTOS</a></p>
	      	</center>
	      </div>
    	</div>
      <div class="col-md-4">
      	<div class="well">
        	<center>
         	<h2>LISTA DE <br>USUARIOS</h2>
          	<p><a href="administrar_usuarios.php"><img src="img/usuarios.png" alt="endoscopia" class="img-circle" width="200" height="200"></a></p>
          	<p><a class="btn btn-primary" href="administrar_usuarios.php" role="button">ADMINISTRAR USUARIOS</a></p>
        	</center>
        </div>
      </div>
        <div class="col-md-4">
        	<div class="well">
          	<center>
	         	<h2>EDITAR  <br>FACTURA</h2>
	          	<p><a href="administrar_impresion.php"><img src="img/imprimir.png" alt="donacion" class="img-circle" width="200" height="200"></a></p>
	          	<p><a class="btn btn-primary" href="administrar_impresion.php" role="button">EDITAR HOJA</a></p>
          	</center>
          </div>
       </div>			
    </div>
      <hr>
      <center><a href="home.php" class="btn btn-warning btn-lg" role="button">VOLVER A INICIO</a></center>
      <br>
      <br>
</div>

</body>
  
</html>