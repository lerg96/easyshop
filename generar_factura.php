<html>
	<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Administracion de Negocios" content="">
    <meta name="Pequeñas empresas" content="">
    <link rel="icon" href="img/logo.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

  <?php
    session_start();
    ob_start();
    
    //verifico el inicio de sesión
    if($_SESSION['sesion_exito']<>1){header('Location:index.php');}

    include("abrir_conexion.php"); 

    if(isset($_GET['id_factura'])){$id_factura=$_GET['id_factura'];$id_parametro_factura=2;}
    else{header('Location:seleccionar_mesa.php?error=5');} //despacho si no tengo el ID

    echo '<title>Imprimir - Factura '.$id_factura.'</title>';

    //consulto los datos de la tabla factura
    $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db5 WHERE id = '$id_factura'");
    while($consulta = mysqli_fetch_array($resultados))
    {
    	$fecha=$consulta['fecha'];
    	$responsable=$consulta['responsable'];
    	$mesa=$consulta['mesa'];
    	$total=$consulta['total'];
    	$impuesto_factura=$consulta['impuesto'];
    }

    list($year, $mes, $dia)=explode("-", $fecha);
    $fecha = $dia."/".$mes."/".$year; 


    $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db7 WHERE id = $id_parametro_factura");
		while($consulta = mysqli_fetch_array($resultados))
		  {
		    //recibo los parametros de la factura
		    	$titulo =			$consulta['titulo'];
				$size_titulo =		$consulta['size_titulo'];
				$nit =				$consulta['nit'];
				$nit_activar =		$consulta['nit_activar'];
				$direccion =		$consulta['direccion'];
				$direccion_activar =$consulta['direccion_activar'];
				$telefono =			$consulta['telefono'];
				$telefono_activar =	$consulta['telefono_activar'];
				$tipo =				$consulta['tipo'];
				$tipo_desactivar =	$consulta['tipo_desactivar'];
				$linea1_activar =	$consulta['linea1_activar'];
				$tipo_tabla =		$consulta['tipo_tabla'];
				$mensaje_propina =	$consulta['mensaje_propina'];
				$mensaje_legal =	$consulta['mensaje_legal'];
				$linea2_activar =	$consulta['linea2_activar'];
				$size_general =		$consulta['size_general'];
				$ancho_papel =		$consulta['ancho_papel'];
				$nombre_impuesto =  $consulta['nombre_impuesto'];
        		$impuesto =         $consulta['impuesto'];
        		$impuesto_activar = $consulta['impuesto_activar'];
		  }
    ?>

		<!-- 256px es igual a 8cm, 1mm=3.2px-->
		  <style type="text/css">
		  	#imprimir {
				width: <?php echo $ancho_papel.'px'; ?>;
				}
		  </style>

	</head>
	
<body>
	<div id="imprimir"> <!-- todo a ancho de 256-->
	  <div class="row">
      <div class="col-sm-12 text-center">
      	<br>
     		<font size="<?php echo $size_general; ?>"> 

		     	<strong><font size=<?php echo '"'.$size_titulo.'">'.$titulo; ?></font></strong><br>
		     	<?php 
		     		if($nit_activar==1){echo '<b>NIT:</b> '.$nit.'<br>';} 
		     		if($direccion_activar==1){echo $direccion.'<br>';} 
		     		if($telefono_activar==1){echo $telefono.'<br>';}
		     		if($tipo_desactivar==1){echo '<b>'.$tipo.'</b>';}
		     		if($linea1_activar==1){echo '<hr>';}
		     	?>

		      <b>Factura No.</b> <?php echo $id_factura; ?>&nbsp;
		      <b>Mesa:			</b> <?php echo $mesa; ?> 					<br>
		      <b>Vendedor:	</b> <?php echo $responsable; ?><br>
		      <b>Fecha:			</b> <?php echo $fecha; ?> 				<br><br>			
		    	
		    	<table <?php echo $tipo_tabla; ?>>
			    	<tr>
			    		<th><center><font size="<?php echo $size_general; ?>">Cant 				</font></center></th>
			    		<th><center><font size="<?php echo $size_general; ?>">Descripción </font></center></th>
			    		<th><center><font size="<?php echo $size_general; ?>">Precio 			</font></center></th>
			    		<th><center><font size="<?php echo $size_general; ?>">Total 			</font></center></th>
			    	</tr>

			    	<?php
			    	$total_general=0;
			    		//consulto los datos de la tabla factura
					    $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db4 WHERE id_factura = '$id_factura'");
					    while($consulta = mysqli_fetch_array($resultados))
					    {
					    	echo'
					    		<tr>
						    		<td><center><font size='.$size_general.'>'.$consulta['cantidad'].'</font></center></td>
						    		<td><font size='.$size_general.'>'.$consulta['nombre'].'</font></td>
						    		<td><font size='.$size_general.'>'.number_format($consulta['precio_unitario'], 0, ",", ".").'</font></td>
						    		<td><font size='.$size_general.'>'.number_format($consulta['precio_total'], 0, ",", ".").'</font></td>
						    	</tr>
					    	';
					    	$total_general=$total_general+$consulta['precio_total'];
					    }
					    
					    if($impuesto_activar==1)
					    {
					    	$total_general2=$total_general+$impuesto_factura;
					    	echo '
						    	<tr>
						    		<td colspan="2"><p class="text-right"><b>SUB-TOTAL:</b></p></td>
						    		<td colspan="2"><p class="text-right"><b>'.number_format($total_general, 0, ",", ".").'</b></p></center></td>
						    	</tr>
						    	<tr>
						    		<td colspan="2"><b><p class="text-right">'.$nombre_impuesto.' '.$impuesto.'%</b></p></td>
						    		<td colspan="2"><p class="text-right"><b>'.number_format($impuesto_factura, 0, ",", ".").'</b></p></center></td>
						    	</tr>
						    	<tr>
						    		<td colspan="2"><p class="text-right"><b>TOTAL:</b></p></td>
						    		<td colspan="2"><p class="text-right"><b>'.number_format($total_general2, 0, ",", ".").'</b></p></center></td>
						    	</tr>
					    	';
					    }
					    else
					    {
					    echo '

					    	<tr>
					    		<td colspan="2"><center><b>TOTAL:</b></center></td>
					    		<td colspan="2"><p class="text-right"><b>'.number_format($total_general, 0, ",", ".").'</b></p></center></td>
					    	</tr>
					    ';
					    }
			    	?>		    	
		    	</table>

		    	<?php 
		    		echo $mensaje_propina;
		    		if($mensaje_propina<>""){echo '<br><br>';}//Si hay texto agrega salto de linea
		    		echo $mensaje_legal;
		    		if($linea2_activar==1){echo '<hr>';}
		    	?>
		    	<strong>
		    		¡GRACIAS POR SU VISITA!<br>
		    		Software: BuyMe
		    	</strong>

		    </font> 
		  </div>
    </div>
  </div><!--Cierro el ancho de la pagina-->
  <?php include("cerrar_conexion.php"); ?>
</body>
</html>