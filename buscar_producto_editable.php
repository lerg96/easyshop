<?php
//Archivo de conexión a la base de datos
require('abrir_conexion.php');

//Variable de búsqueda
$consultaBusqueda = $_POST['valorBusqueda'];

//Filtro anti-XSS
//$caracteres_malos = array("<", ">", "\"", "'", "/", "<", ">", "'", "/");
//$caracteres_buenos = array("& lt;", "& gt;", "& quot;", "& #x27;", "& #x2F;", "& #060;", "& #062;", "& #039;", "& #047;");
//$consultaBusqueda = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda);

//Variable vacía (para evitar los E_NOTICE)
$constructor = "";
$mensaje = "";
$i=0;

//Comprueba si $consultaBusqueda está seteado
if (isset($consultaBusqueda)) {

	//Selecciona todo de la tabla mmv001 
	//donde el nombre sea igual a $consultaBusqueda, 
	//o el apellido sea igual a $consultaBusqueda, 
	//o $consultaBusqueda sea igual a nombre + (espacio) + apellido
	$consulta = mysqli_query($conexion, "SELECT * FROM $tabla_db2
	WHERE descripcion COLLATE UTF8_SPANISH_CI LIKE '%$consultaBusqueda%'");

	//Obtiene la cantidad de filas que hay en la consulta
	$filas = mysqli_num_rows($consulta);

	//Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
	if ($filas === 0) {
		$mensaje = '<h3><center><div class="alert alert-danger"><b>NO HAY PRODUCTOS CON ESA DESCRIPCIÓN</b>  </div></center></h3>';
	} else {

		$constructor = '
		<table class="table table-bordered table-striped table-hover" width="100%">
          <tr>
            <th width="10%"><center>Codigo</center></th>
            <th width="50%"><center>Producto</center></th>
            <th width="10%"><center>Precio</center></th>
            <th width="15%"><center>Editar</center></th>
            <th width="15%"><center>Eliminar</center></th>
          </tr>
		';

		//La variable $resultado contiene el array que se genera en la consulta, así que obtenemos los datos y los mostramos en un bucle
		while($resultados = mysqli_fetch_array($consulta)) 
		{

			//Output
			$mensaje .= '
				<tr>
                  <td>
                  	<center>'.$resultados['codigo'].'</center>
                  </td>
                  <td>
                  	<center>
                  		<input type="text" class="form-control" id="d'.$resultados['codigo'].'" value="'.$resultados['descripcion'].'" Disabled>
                  	</center>
                  </td>
                  <td>
                  	<center>
                  		<input type="text" class="form-control" id="p'.$resultados['codigo'].'" value="'.number_format($resultados['precio'], 0, ",", ".").'" Disabled>
                  	</center>
                  </td>
                   <td>
                    <center>
                      <div id="editar'.$resultados['codigo'].'" class="show">
                        <button type="button" href="#" class="btn btn-primary btn-xs" onclick="editar(this)" id="'.$resultados['codigo'].'">EDITAR</button>
                      </div>
                      <div id="guardar'.$resultados['codigo'].'" class="hidden">
                        <button type="button" class="btn btn-success " onclick="guardar(this)" id="'.$resultados['codigo'].'">GUARDAR</button>
                      </div>
                    </center>
                  </td>
                  <td>
                  	<center>
                      <button type="button" class="btn btn-danger btn-xs"  data-toggle="modal" data-target=".eliminar_producto" id="'.$resultados['codigo'].'" onclick="detectar_producto(this)">ELIMINAR</button>

                  		<!--<a role="button" href="agregar_productos.php?del=1&codigo='.$resultados['codigo'].'" class="btn btn-danger btn-xs">ELIMINAR</a>-->
                  	</center>
                  </td>
                 
                </tr>';
                $i++;
                if($i>=50){break 1;}

		};//Fin while $resultados

	}; //Fin else $filas

};//Fin isset $consultaBusqueda

//Devolvemos el mensaje que tomará jQuery
echo $constructor;
echo $mensaje;
echo '<i><font size="2" color="#777">*Mostrando Maximo 50 datos</font></i>';

?>