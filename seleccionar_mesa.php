<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Administracion de Negocios" content="">
    <meta name="Pequeñas empresas" content="">
    <link rel="icon" href="img/logo.ico">
    <title> Gestion de Pedidos</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- Bootstrap core CSS -->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>
<body>
  <?php
    session_start();
    ob_start();
    
    include("abrir_conexion.php");

    //verifico el inicio de sesión
    if($_SESSION['sesion_exito']<>1){header('Location:index.php');}

    if(isset($_GET['error'])){$error=$_GET['error'];}else{$error="";}
  ?>
<div class="container">

		<div class="row">
			<div class="col-lg-12 text-center">
				<h1>GESTIONAR PEDIDOS</h1>
				<p class="lead">DELICIAS BURGER</p>
				<hr>
			</div>
		</div>
    
    <h3><center><strong>
      <?php
        echo '<p class="bg-danger">';
          if($error=="1"){echo "POR FAVOR, DIGITA UN NUMERO DE MESA";} 
          if($error=="4"){echo "EL NUMERO DE MESA ES INVALIDO";} 
          if($error=="3"){echo "ERROR, LA FACTURA YA ESTABA GUARDADA";}
          if($error=="5"){echo "ERROR AL GENERAR LA FACTURA";}
        echo '</p>';
        echo '<p class="bg-success">';
          if($error=="2"){echo "VENTA CANCELADA CON EXITO";}
          
        echo '</p>';        
      ?>
    </strong></center></h3>

	<!--Inicio del formulario de Iniciar sesion-->
	<div class="row">
	  <div class="col-md-4"></div>
	  <div class="col-md-4">
	      <div class="well"> <!--hace un sombreado a la columna-->
	          <center>
	              <h3><strong>DELICIAS BURGER</strong></h3><br>  
	              <img src="img/burger.jpg" class="img-rounded" width="150" height="150">
	              <br><br>
	              <form class="form-inline" method="POST" action="administrar_venta.php" name="mesa">
	                  <div class="form-group">
	                    <label for="usuario">NUMERO DE PEDIDO</label>  
	                    <br>  
                      <input type="number" class="form-control" id="nueva_mesa" placeholder="" name="no_mesa">
	                  </div>
	                  <br><br>
	                  <p>
	                  	<input type="submit" id="enviar" class="btn btn-success" value="NUEVA VENTA" name="btn_index">
	                   	<a class="btn btn btn-danger" href="home.php" role="button">CANCELAR</a>
	                  </p>
	              </form>
	          </center>  
             
	      </div>
	  </div>
	  <div class="col-md-4"></div>
	</div>
	<hr>
  <div class="row">
    <?php    
          $resultados = mysqli_query($conexion,"SELECT DISTINCT mesa FROM $tabla_db3");
          while($consulta = mysqli_fetch_array($resultados))
          {
            echo '
            <div class="col-md-2">
              <center>
                <a href="administrar_venta.php?no_mesa='.$consulta['mesa'].'">
                  <h3>'.$consulta['mesa'].'</h3>
                  <p><img src="img/pedido.png" width="100" height="100"></p>
                </a>
              </center>
            </div>
          ';
          }
    include("cerrar_conexion.php");
    ?>
  </div>


</div>      
	</div>

</body>
  
</html>