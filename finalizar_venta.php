<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Administracion de Negocios" content="">
    <meta name="Pequeñas empresas" content="">
    <link rel="icon" href="img/logo.ico">
    <title>Finalizar Venta</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- Bootstrap core CSS -->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--Calculo los totales de la hoja-->
</head>
<body>

<?php
session_start();
ob_start();
include("abrir_conexion.php");

//Si no inicia sesion. ¡Chao papá!
if ($_SESSION['sesion_exito'] <> 1) {
    header('Location:index.php');
}

$accion = $_GET['anular'];//1 para anular SIN guardar factura, 2 Eliminar desde informes, 0 para guardar nueva


if ($accion == 1) {
    $mesa = $_GET['mesa'];  //numero de mesa
    $_DELETE_SQL = "DELETE FROM $tabla_db3 WHERE mesa = '$mesa'";
    mysqli_query($conexion, $_DELETE_SQL);

    header('Location:seleccionar_mesa.php?error=2');
}
if ($accion == 2) {
    $id = $_GET['id_factura'];  //id de factura a eliminar
    $fecha_inicial = $_GET['fecha_inicial'];
    $fecha_final = $_GET['fecha_final'];

    //Elimino PRIMERO todos los productos vendidos de la factura
    $_DELETE_SQL = "DELETE FROM $tabla_db4 WHERE id_factura = '$id'";
    mysqli_query($conexion, $_DELETE_SQL);

    //Elimino la factura
    $_DELETE_SQL = "DELETE FROM $tabla_db5 WHERE id = '$id'";
    mysqli_query($conexion, $_DELETE_SQL);

    header('Location:generar_informe.php?del=0&tipo=i&fecha_inicial=' . $fecha_inicial . '&fecha_final=' . $fecha_final . '&codigo=' . $id . '&error=1');


}
if ($accion == 0) {
    $mesa = $_GET['mesa'];  //numero de mesa
    $total_general = 0;
    $impuesto = 0;
    $ganancia_total = 0;

    //sumo para hallar el total de la factura
    $resultados = mysqli_query($conexion, "SELECT * FROM $tabla_db3 WHERE mesa = $mesa");
    while ($consulta = mysqli_fetch_array($resultados)) {
        $total_general = $total_general + $consulta['precio_total'];
        $ganancia_total = $ganancia_total + $consulta['ganancia'];
    }

    //hallo el porcentaje del impuesto
    $resultados = mysqli_query($conexion, "SELECT * FROM $tabla_db7 WHERE id = '2'");
    while ($consulta = mysqli_fetch_array($resultados)) {
        $impuesto = $consulta['impuesto'];
        $impuesto_activar = $consulta['impuesto_activar'];
    }

    //Si es igual a 0, me actualizaron la pagina
    if ($total_general == 0) {
        header('Location:seleccionar_mesa.php?error=3');
    } else {
        //calculo el impuesto
        if ($impuesto_activar == 0) {
            $impuesto = 0;
        } else {
            $impuesto = $total_general * ($impuesto / 100);
        }


        $fecha = date("Y-m-d", time());
        $responsable = $_SESSION['nombre_usuario'];
        //inserto en la tabla facturas el ID, la fecha y el total
        mysqli_query($conexion, "INSERT INTO $tabla_db5 (fecha,total, ganancia,impuesto,responsable,mesa) values ('$fecha','$total_general','$ganancia_total','$impuesto','$responsable','$mesa')");

        //extraigo el ID de la factura creada
        $id_factura = mysqli_insert_id($conexion);

        $resultados = mysqli_query($conexion, "SELECT * FROM $tabla_db3 WHERE mesa = $mesa");
        while ($consulta = mysqli_fetch_array($resultados)) {
            $nombre = $consulta['nombre'];
            $codigo = $consulta['codigo'];
            $cantidad = $consulta['cantidad'];
            $precio_unitario = $consulta['precio'];
            $precio_total = $consulta['precio_total'];
            $ganancia = $consulta['ganancia'];

            mysqli_query($conexion, "INSERT INTO $tabla_db4 (id_factura,mesa,codigo,nombre,cantidad,precio_unitario,precio_total,ganancia) values ('$id_factura','$mesa','$codigo','$nombre','$cantidad','$precio_unitario','$precio_total','$ganancia')");
            mysqli_query($conexion, "UPDATE $tabla_db2 SET  cantidad =  cantidad - '$cantidad' WHERE codigo = '$codigo'");
        }

        $_DELETE_SQL = "DELETE FROM $tabla_db3 WHERE mesa = '$mesa'";
        mysqli_query($conexion, $_DELETE_SQL);
    }
}
?>


<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>FINALIZAR VENTA</h1>
            <p class="lead"><strong>Resultados</strong></p>
            <hr>
        </div>
    </div>
    <br>
    <?php
    echo '<h3><center><strong><p class="bg-success">';
    if ($accion == 0) {
        echo "FACTURA GUARDADA CON EXITO";
    }
    if ($accion == 1) {
        echo "VENTA CANCELADA CON EXITO";
    }
    if ($accion == 2) {
        echo "FACTURA ELIMINADA CON EXITO";
    }
    echo '</p></strong></center></h3>';

    echo '<h3><center><strong><p class="bg-danger">';
    if ($accion == 3) {
        echo "LA FACTURA YA ESTA GUARDADA";
    }
    echo '</p></strong></center></h3>';

    ?>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="well">
                <center>
                    <h2><p>¿DESEA IMPRIMIR?</p></h2>
                    <center>
                        <img src="img/imprimir.png" alt="AREA VENTAS" class="img-circle" width="200" height="200">
                    </center>
                    <h2>NUMERO DE MESA:
                        <br>
                        <?php echo $mesa; ?>
                    </h2>
                    <br>
                    <a class="btn btn-warning btn-lg" role="button" href="seleccionar_mesa.php">VOLVER VENTAS</a>
                    <a class="btn btn-warning btn-lg" role="button" href="seleccionar_pedidos.php">VOLVER PEDIDOS</a>
                    <a class="btn btn-success btn-lg" role="button"
                       href="generar_factura.php?id_factura=<?php echo $id_factura; ?>" target="_blank">IMPRIMIR </a>
                    </form>
                </center>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <?php
    if ($accion == 2) {
        echo '
          <center>
            <a class="btn btn-warning btn-lg" role="button" href="generar_informe.php?del=0&tipo=i&fecha_inicial=' . $fecha_inicial . '&fecha_final=' . $fecha_final . '&codigo=' . $id . '">VOLVER A INFORMES</a>
          </center>
          ';
    }
    ?>


</div> <!-- /container -->
<script src=https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js></script>
</body>
</html>

