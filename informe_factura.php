<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Diseño y Publicidad" content="">
    <meta name="Central de Diseño" content="">
    <link rel="icon" href="img/potato.ico">
    <title>Ventas - Buyme</title>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="./css/navbar-fixed-top.css" rel="stylesheet">-->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./js/ie-emulation-modes-warning.js"></script>
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./js/ie10-viewport-bug-workaround.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--Calculo los totales de la hoja-->
    <script>
     var ftotal1=0;     
        function multiplicar1() 
        {
          cantidad = document.getElementById("cantidad1").value;
          unitario = document.getElementById("unitario1").value;  
          ftotal1 = cantidad*unitario;
          document.getElementById("total1").value = ftotal1.toLocaleString();      
        }          
    </script>
  </head>
<body>
  <?php
    session_start();
    ob_start();
    include("abrir_conexion.php"); 

    //Si no inicia sesion. ¡Chao papá!
    if($_SESSION['sesion_exito']<>1){header('Location:index.php');} //Si NO inicio sesion, ¡hasta luego!
    if($_SESSION['tipo_usuario']<>"A"){header('Location:index.php');}//Si NO es administrador, Chao mijo!

    //si no tengo ningun valor en la variable MESA, me devuelve a otra pagina
    if(isset($_GET['codigo']))
    {
      $id = $_GET['codigo'];
      $responsable = $_GET['responsable'];
      $fecha_inicial = $_GET['fecha_inicial'];
      $fecha_final = $_GET['fecha_final'];
    }

    $total_general=0;
    $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db4 WHERE id_factura = $id");
    while($consulta = mysqli_fetch_array($resultados))
    {
      $total_general=$total_general+$consulta['precio_total'];
    }
  ?>

<div class="container">
 <form method="POST" action="administrar_venta.php" name="nueva_venta">
 <input type="hidden" name="agregar_producto" id="existe" value="1">
  <input type="hidden" name="no_mesa" id="mesa" value="<?php echo $mesa; ?>">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1>ADMINISTRAR VENTA</h1>
        <p class="lead">Responsable:<strong> <?php echo $responsable; ?></strong></p>
        <hr>
      </div>
    </div>
    <br>

    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        <div class="well">
          <center>
            <h1><strong>SALDO TOTAL<br><?php echo number_format($total_general, 0,',','.'); ?></strong></h1>
          </center>
        </div>
      </div>
      <div class="col-md-4"></div>
    </div>
    

    <br>
    <hr>
  <!--Termina AÑADIR PRODUCTOS-->
        <a name="borrar"></a>
        <center><h2><b>LISTA DE PRODUCTOS</b></h2></center>
        
        <div class="well">
          <!-- TERMINA TABLA DE PRODUCTOS!-->
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
              <div class="table-responsive"> 
                <table class="table table-bordered table-striped" width="60%">
                  <tr>
                    <td width="10%"><center><strong>CANTIDAD</strong></center></td>
                    <td width="55%"><center><strong>DESCRIPCIÓN</strong></center></td>
                    <td width="15%"><center><strong>VALOR UNITARIO</strong></center></td>
                    <td width="20%"><center><strong>VALOR TOTAL</strong></center></td>
                  </tr>
                  <?php
                    $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db4 WHERE id_factura = $id");
                    while($consulta = mysqli_fetch_array($resultados))
                    {echo '
                      <tr>
                        <td><input type="text" class="form-control" value="'.$consulta['cantidad'].'" disabled></td>
                        <td><input type="text" class="form-control" value="'.$consulta['nombre'].'" disabled></td>
                        <td><input type="text" class="form-control" value="'.number_format($consulta['precio_unitario'], 0, ",", ".").'" disabled></td>
                        <td><input type="text" class="form-control" value="'.number_format($consulta['precio_total'], 0, ",", ".").'" disabled></td>
                      </tr>
                      ';
                    }
                  ?>
                 </table>
              </div>
            </div>
          </div>    
        </div>
        <center>
          <a href="generar_informe.php?del=0&tipo=i&fecha_inicial=<?php echo $fecha_inicial ?>&fecha_final=<?php echo $fecha_final ?>&codigo=<?php echo $id ?>" class="btn btn-warning btn-lg" role="button">VOLVER A INFORMES</a>
          <a class="btn btn-danger btn-lg" role="button" data-toggle="modal" data-target=".anular">ANULAR VENTA</a>
          
        </center>

        <br><br>

<!--En caso de que de clic sobre ANULAR VENTA-->
  <div class="modal fade anular" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">


        <div class="modal-header">
          <h4 class="modal-title" id="mySmallModalLabel">ANULAR FACTURA</h4>
        </div>
        <div class="modal-body">
          <p> 
            <font color="RED" align="center">
              Al <strong>ANULAR</strong> se perdera para siempre la informacion de esta factura. 
            </font>
            <br><br>
            La factgura con <strong>ID <?php echo $id; ?></strong> se eliminara y <strong>JAMAS PODRA RECUPERARLA</strong>
            <br>
            <h2>¿Desea Anularla?</h2>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancelar</button>
          <a class="btn btn-danger btn-lg" role="button" href="finalizar_venta.php?anular=2&id_factura=<?php echo $id?>&fecha_inicial=<?php echo $fecha_inicial?>&fecha_final=<?php echo $fecha_final?>">ANULAR FACTURA</a>
          </div>
      </div>
    </div>
  </div>
<!--TERMINA En caso de que de clic sobre ANULAR VENTA-->

  </form>
  </div><!--Containder-->
<?php include("cerrar_conexion.php"); ?>

</body>
</html>
