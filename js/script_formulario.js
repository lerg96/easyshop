      $(document).ready(function(){
        //Desabilito la tecla ENTER, para que no se equivoquen
         $("form").keypress(function(e) {
            if (e.which == 13) {
                return false;
            }
        });

        
        $('.f_presentacion').hide();     
        $('.f_volantes').hide();     
        $('.f_talonarios').hide();     
        $('.f_membrete').hide();     
        //$('.f_afiche').hide();     
        $('.f_folletos').hide(); 
        $('.f_agendas').hide();
        $('.t_brillantes').hide();
        $('.t_mate').hide();   
        $('.volante_propalcote').hide();
        $('.volante_bond').hide(); 
        $('.talonario_bond').hide();
        $('.talonario_quimico').hide();
        $('.f_laser').hide();
        $('.papeleria').hide();     
        $('.gran_formato').hide(); 
        $('.tampografia').hide(); 
        $('.otro').hide();  
        $('.f_plotter').hide();   

      });   

      function gran_formato(tipo_gran_formato) {
          if(document.getElementById('tipo_gran_formato').value!="Ploter de Corte")
            {
              $('.f_plotter').hide(500);
            }
          if(document.getElementById('tipo_gran_formato').value=="Ploter de Corte")
            {
              $('.f_plotter').show(500);
            }
          }

        function funcion_ocultar(tipo_trabajo) {
          if(document.getElementById('tipo_trabajo').value=="")
            {
              $('.papeleria').hide();
              $('.gran_formato').hide();
              $('.tampografia').hide();
              $('.otro').hide();
              document.getElementById('cantidad').disabled = false;
            }

        
          if(document.getElementById('tipo_trabajo').value=="Papeleria")
            {
              $('.papeleria').show(500);
              $('.gran_formato').hide(500);
              $('.tampografia').hide(500);
              $('.otro').hide(500);
              document.getElementById('cantidad').disabled = false;
            }

          if(document.getElementById('tipo_trabajo').value=="Gran Formato")
            {
              $('.papeleria').hide(500);
              $('.gran_formato').show(500);
              $('.tampografia').hide(500);
              $('.otro').hide(500);
              document.getElementById('cantidad').disabled = true;
            }

          if(document.getElementById('tipo_trabajo').value=="Tampografia")
            {
              $('.papeleria').hide(500);
              $('.gran_formato').hide(500);
              $('.tampografia').show(500);
              $('.otro').hide(500);
              document.getElementById('cantidad').disabled = false;
            }
        }

         function ocultar(tipo_papeleria) {
          if(document.getElementById('tipo_papeleria').value=="")
            {
              $('.f_presentacion').hide(500);
              //$('.t_brillantes').show(500);//Por defecto aparecera  brillantes
              $('.f_volantes').hide(500);
              $('.f_talonarios').hide(500);
              $('.f_membrete').hide(500);
              //$('.f_afiche').hide(500);
              $('.f_folletos').hide(500);
              $('.f_agendas').hide(500);
              $('.f_laser').hide(500);
            }

          if(document.getElementById('tipo_papeleria').value=="Tarjeta de Presentacion")
            {
              $('.f_presentacion').show(500);
              //$('.t_brillantes').show(500);//Por defecto aparecera  brillantes
              $('.f_volantes').hide(500);
              $('.f_talonarios').hide(500);
              $('.f_membrete').hide(500);
              //$('.f_afiche').hide(500);
              $('.f_folletos').hide(500);
              $('.f_agendas').hide(500);
              $('.f_laser').hide(500);
            }
          
          if(document.getElementById('tipo_papeleria').value=="Volantes")
            {
              $('.f_presentacion').hide(500);
              $('.f_volantes').show(500);
              $('.f_talonarios').hide(500);
              $('.f_membrete').hide(500);
              //$('.f_afiche').hide(500);
              $('.f_folletos').hide(500);
              $('.f_agendas').hide(500);
              $('.f_laser').hide(500);
            }

          if(document.getElementById('tipo_papeleria').value=="Talonarios")
            {
              $('.f_presentacion').hide(500);
              $('.f_volantes').hide(500);
              $('.f_talonarios').show(500);
              $('.f_membrete').hide(500);
              //$('.f_afiche').hide(500);
              $('.f_folletos').hide(500);
              $('.f_agendas').hide(500);
              $('.f_laser').hide(500);
            }

          if(document.getElementById('tipo_papeleria').value=="Hojas Membrete")
            {
              $('.f_presentacion').hide(500);
              $('.f_volantes').hide(500);
              $('.f_talonarios').hide(500);
              $('.f_membrete').show(500);
              //$('.f_afiche').hide(500);
              $('.f_folletos').hide(500);
              $('.f_agendas').hide(500);
              $('.f_laser').hide(500);
            }


          if(document.getElementById('tipo_papeleria').value=="Folletos y Afiches")
            {
              $('.f_presentacion').hide(500);
              $('.f_volantes').hide(500);
              $('.f_talonarios').hide(500);
              $('.f_membrete').hide(500);
              //$('.f_afiche').hide(500);
              $('.f_folletos').show(500);
              $('.f_agendas').hide(500);
              $('.f_laser').hide(500);
            }

          if(document.getElementById('tipo_papeleria').value=="Agendas")
            {
              $('.f_presentacion').hide(500);
              $('.f_volantes').hide(500);
              $('.f_talonarios').hide(500);
              $('.f_membrete').hide(500);
              //$('.f_afiche').hide(500);
              $('.f_folletos').hide(500);
              $('.f_agendas').show(500);
              $('.f_laser').hide(500);
            }
            if(document.getElementById('tipo_papeleria').value=="Laser")
            {
              $('.f_presentacion').hide(500);
              $('.f_volantes').hide(500);
              $('.f_talonarios').hide(500);
              $('.f_membrete').hide(500);
              //$('.f_afiche').hide(500);
              $('.f_folletos').hide(500);
              $('.f_agendas').hide(500);
              $('.f_laser').show(500);
            }
        }

//tarjetas de Presentacion

         function tipo_tarjeta_select(tipo_tarjeta) {
          if(document.getElementById('tipo_tarjeta').value=="")
            {
              $('.t_brillantes').hide(500);
              $('.t_mate').hide(500);  
            }

          if(document.getElementById('tipo_tarjeta').value=="Brillantes")
            {
              $('.t_brillantes').show(500);
              $('.t_mate').hide(500);  
            }
          
          if(document.getElementById('tipo_tarjeta').value=="Mate (UV)")
            {
              $('.t_brillantes').hide(500);
              $('.t_mate').show(500);
            }

           if(document.getElementById('tipo_tarjeta').value=="Adhesivas")
            {
              $('.t_brillantes').hide(500);
              $('.t_mate').hide(500);
            }
          }    
        
//Volantes

         function tipo_volante_select(tipo_volante){
          if(document.getElementById('tipo_volante').value=="")
            {
              $('.t_brillantes').hide(500);
              $('.t_mate').hide(500);  
            }

          if(document.getElementById('tipo_volante').value=="Propalcote")
            {
              $('.volante_propalcote').show(500);
              $('.volante_bond').hide(500);  
            }
          
          if(document.getElementById('tipo_volante').value=="Bond")
            {
              $('.volante_propalcote').hide(500);
              $('.volante_bond').show(500);
            }
          }  

//Talonarios

         function tipo_talonario_select(tipo_talonario){
          if(document.getElementById('tipo_talonario').value=="")
            {
              $('.talonario_bond').hide(500);
              $('.talonario_quimico').hide(500);  
            }

          if(document.getElementById('tipo_talonario').value=="Bond")
            {
              $('.talonario_bond').show(500);
              $('.talonario_quimico').hide(500);  
            }
          
          if(document.getElementById('tipo_talonario').value=="Quimico")
            {
              $('.talonario_bond').hide(500);
              $('.talonario_quimico').show(500);  
            }
          }  