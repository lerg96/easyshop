<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Administracion de Negocios" content="">
    <meta name="Pequeñas empresas" content="">
    <link rel="icon" href="img/logo.ico">
    <title>DELICIAS BURGER</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- Bootstrap core CSS -->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  </head>
<body>
  <?php
    session_start();
    ob_start();

    if(isset($_POST['btn_index']))//Verifico que el boton "iniciar sesion" fue oprimido
    {
      $_SESSION['sesion_exito']=0;

      $user = $_POST['user'];
      $pass = $_POST['pass'];

      if($user=="" || $pass=="")
      {
        $_SESSION['sesion_exito']=2;//2 sera error de campos vacios
      }
      else
      {
        include("abrir_conexion.php");  
        $_SESSION['sesion_exito']=3;//3 Datos Incorrectos
        $resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db1 WHERE documento = '$user' AND pass = '$pass'");
        while($consulta = mysqli_fetch_array($resultados))
            {
               $_SESSION['sesion_exito']=1;//Inicio Sesion :D
               $_SESSION['tipo_usuario']=$consulta['rol'];
               $_SESSION['nombre_usuario']=$consulta['nombre'];
            }
        include("cerrar_conexion.php");
      }
    }

    if($_SESSION['sesion_exito']<>1)
    {
      header('Location:index.php');
    }
  ?>








<div class="container">

		<div class="row">
			<div class="col-lg-12 text-center">
				<h1>DELICIAS BURGER</h1>
				<p class="lead">Hola <strong><?php echo $_SESSION['nombre_usuario']?></strong></p>
				<hr>
			</div>
		</div>
		<br>
		<div class="row">
	        <div class="col-md-4">
	          	<center>
		          	<h2>ADMINISTRAR VENTAS</h2>
		        	<p><a href="seleccionar_mesa.php"><img src="img/hamburgesa.jpg" alt="Historia Clinica" class="img-circle" width="200" height="200"></a></p>
		          	<p><a class="btn btn-primary" href="seleccionar_mesa.php" role="button">ADMINISTRAR VENTAS</a></p>
		       	</center>
	        </div>
             <div class="col-md-4">

        
               <center>
                  <h2>ADMINISTRAR PEDIDOS</h2>
              <p><a href="seleccionar_pedidos.php"><img src="img/pedidos.jpg" alt="Historia Clinica" class="img-circle" width="200" height="200"></a></p>
                <p><a class="btn btn-primary" href="seleccionar_pedidos.php" role="button">ADMINISTRAR PEDIDOS</a></p>
                </center>
	        </div>
          <div class="col-md-4">
	          	<center>
		         	<h2>CERRAR SESION</h2>
		          	<p><a href="cerrar_sesion.php"><img src="img/salir2.png" alt="informe" class="img-rounded" width="200" height="200"></a></p>
		          	<p><a class="btn btn-danger" href="cerrar_sesion.php" role="button">CERRAR SESIÓN</a></p>
	          	</center>
	       </div>
      </div>
      <br><br>
      
      <?php 
      if($_SESSION['tipo_usuario']=="A")
      {
	    echo ' <div class="row">
		        <div class="col-md-2"></div>
		        <div class="col-md-4">
		          	<center>
			         	<h2>ADMINISTRACIÓN</h2>
			          	<p><a href="admin.php"><img src="img/admin.jpg" alt="admin" class="img-circle" width="200" height="200"></a></p>
			          	<p><a class="btn btn-primary" href="admin.php" role="button">ADMINISTRACIÓN</a></p>
		          	</center>
                </div>
                <div class="col-md-4">
                  <center>
                <h2>EGRESOS</h2>
                <p><a href="administrar_egresos.php"><img src="img/egreso.jpg" class="img-circle" width="200" height="200"></a></p>
                <p><a class="btn btn-primary" href="administrar_egresos.php" role="button">ADMINISTRAR EGRESOS</a></p>
              </center>
           </div>       
           <div class="col-md-2"></div>
		           
           
	      </div>';
	    }
      else
      {
        echo ' <div class="row">
            <div class="col-md-4"></div>      
           <div class="col-md-4">
                  <center>
                <h2>EGRESOS</h2>
                <p><a href="administrar_egresos.php"><img src="img/egreso.jpg" class="img-circle" width="200" height="200"></a></p>
                <p><a class="btn btn-primary" href="administrar_egresos.php" role="button">ADMINISTRAR EGRESOS</a></p>
              </center>
           </div>       
           <div class="col-md-2"></div>
        </div>';
      }
	    ?>
	</div>
  <br>
</body>
  
</html>