<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Diseño y Publicidad" content="">
    <meta name="Central de Diseño" content="">
    <link rel="icon" href="img/favicon.ico">
    <title>Buyme - Administrar Egresos</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- Bootstrap core CSS -->
    <!--<link href="./css/navbar-fixed-top.css" rel="stylesheet">-->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--JavaScrip para las FECHAS-->
  <script>
      $(function(){
        $('.datepicker').datepicker();
      });
    </script>

  </head>
<body>
  <?php
    session_start();
    ob_start();
  ?>
<div class="container">
 <input type="hidden" name="agregar_producto" id="existe" value="1">
  <input type="hidden" name="no_mesa" id="mesa" value="<?php echo $mesa; ?>">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h1>BUYME VERSIÓN 1.0</h1>
        <p class="lead"><strong>BETA</strong></p>
        <hr>
      </div>
    </div>
  <div class="well"> 
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-4">         
        
           <h2><center><strong>¿BUYME?</strong></center></h2>
           <p class="text-justify">Buyme* fue creado por la necesidad de un programa <strong>GRATUITO e intuitivo</strong> para administrar
           empresas que cuentan con pocos recursos y así poder llevar una contabilidad ordenada y satisfactoria. </p>
          <p>*Usa codigo de Bootstrap, Jquery, Php y Mysql.</p>
          <center>  
            <a target="_blank" href="https://www.facebook.com/dostin.hurtado/">
              <img src="img/facebook.png" width="40" height="40">
            </a>
            
            <a target="_blank" href="https://www.youtube.com/dostinhurtado/">
              <img src="img/youtube.png" width="48" height="48">
            </a>
            
            <a target="_blank" href="https://www.instagram.com/dostin.hurtado/">
              <img src="img/instagram.png" width="43" height="43">
            </a>
           
            <a target="_blank" href="https://www.twitter.com/dostin_hurtado/">
              <img src="img/twitter.png" width="50" height="50">
            </a>
          </center>
          <br><hr>
           <h2><center><strong>¿GRATIS?</strong></center></h2>
            <p class="text-justify">
              <b>¿Vez algún anuncio publicitario?</b>, al igual que Wikipedia este programa funciona a través de <b>DONACIONES</b>.
              <br>
              Si le ha sido de ayuda, recuerda que puedes realizar una DONACIÓN como agradecimiento a este gran proyecto.
            </p>
           <center><a class="btn btn-success" target="_blank" href="http://www.dostinhurtado.com/main/donativo" role="button">METODOS DE DONACIÓN</a></center>
      </div>
      <div class="col-md-4">   
          <h2><center><strong>¿NECESITAS AYUDA?</strong></center></h2>
             <p class="text-justify">
             Sé que no todos están familiarizados con programas para administrar una empresa, asi que realice una serie de 
             tutoriales para que puedan conocer y manejar todas las secciones <b>sin ayuda de terceros</b>, además que dejo
             los datos de contacto para cualquier posible error y futuras actualizaciones.
             </p>
             <center><a class="btn btn-success" target="_blank" href="http://buyme.dostinhurtado.com" role="button">VER TUTORIALES</a></center>
        <hr>
          <h2><center><strong>¡CURSOS GRATIS!</strong></center></h2>
           <p class="text-justify">
           Recuerda que yo <b>Dostin Hurtado</b> doy cursos en línea de diferentes áreas de conocimiento también de forma <b>GRATUITA, SIN REGISTROS NI MATRICULAS</b> para que se capacite y pueda mejorar o adquirir una estabilidad laboral. Todo esto es gracias a sus donaciones :)
           </p>
           <center><a class="btn btn-success" target="_blank" href="http://www.dostinhurtado.com/main/cursos" role="button">VER CURSOS</a></center>
      </div>
      <div class="col-md-2"></div>
    </div>
    <br><br>
    <div class="row">         
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <h2><center><strong>¡CONTACTEME!</strong></center></h2>
          <center>
            <a target="_blank" href="http://www.dostinhurtado.com/main/contacto">
              <img src="img/yo.jpg" alt="admin" class="img-circle" width="200" height="200">
            </a>
          </center>
          <p class="text-justify">
            Puede usar mi página web oficial para tener un contacto directo conmigo, ten en cuenta que a pesar de leer todos los correos no siempre dispongo del tiempo para atender todos los requerimientos.
          </p>
        <center>
          <a class="btn btn-primary" target="_blank" href="http://www.dostinhurtado.com/main/contacto" role="button">CONTACTAR</a>
        </center>
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
  <center><a class="btn btn-warning btn-lg" href="home.php" role="button">VOLVER A INICIO</a></center>
  <br><br>
</div><!--Containder-->
</body>
  <?php include("creador.php"); ?>
</html>
