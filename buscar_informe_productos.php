<?php
  //Archivo de conexión a la base de datos
  require('abrir_conexion.php');

  //Recibo los datos de Javascript Funcion Buscar
  $consultaBusqueda = $_POST['valorBusqueda'];
  $fecha_inicial    = $_POST['fecha_inicial'];
  $fecha_final      = $_POST['fecha_final'];

  //Variable vacía (para evitar errores)
  $constructor = '
  <i><font size="2" color="#777">*No se muestran los productos eliminados</font></i>
  <div class="table-responsive"><table width="80%" class="table table-hover table-bordered"> 
    <tr class="active">
      <th width="15%"><center>NUM. VENTAS</center></th>
      <th width="50%"><center>DESCRIPCIÓN</center></th>
      <th width="35%"><center>TOTAL VENTAS</center></th>
    </tr>';
  $mensaje = "";
  $i=0;

  //Comprueba si $consultaBusqueda está seteado
  if(isset($consultaBusqueda)) 
  {

    //Segun la busqueda que realicen, busco cualqueir coincidencia en las descripciones
    $resultados = mysqli_query($conexion, "SELECT * FROM $tabla_db2 WHERE descripcion COLLATE UTF8_SPANISH_CI LIKE '%$consultaBusqueda%'");
    $filas = mysqli_num_rows($resultados); //Obtiene la cantidad de filas que hay en la consulta

    //si no existen datos, muestro error
    if ($filas === 0){echo'<h3><center><div class="alert alert-danger"><b>NO SE ENCUENTRAN REGISTROS DEL PRODUCTO</b>  </div></center></h3>';} 
    else 
    {
      echo $constructor;
      while($consulta = mysqli_fetch_array($resultados))
      {
        $codigo=$consulta['codigo'];
        $nombre=$consulta['descripcion'];
        $total=0;
        $i=0;

        $resultados2 = mysqli_query($conexion,"SELECT * from $tabla_db5 WHERE anular = '0' AND fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
        while($consulta2 = mysqli_fetch_array($resultados2))
        {
          $id_factura=$consulta2['id'];

          //en la tabla de productos vendidos, relaciono los IDs de los productos 
          $resultados3 = mysqli_query($conexion,"SELECT * from $tabla_db4 WHERE codigo = '$codigo' AND id_factura = '$id_factura'");
          while($consulta3 = mysqli_fetch_array($resultados3))
          {
            //Dinero total recogido de ese producto
            $total=$total+$consulta3['precio_total'];
            //calculo la cantidad de productos que despacho
            $i=$i+$consulta3['cantidad'];
          } 
        }

        //El producto puede existir pero puede que no tenga ventas registradas
        if($total!=0)
        {
          $mensaje= 
          '
            <tr>
              <td><center>'.$i.'</center></td>
              <td>'.$nombre.'</td>
              <td>'.number_format($total, 0, ",", ".").'</td>
            </tr>
          ';
          echo $mensaje;
        }
      }//termina Primer While
      
      $codigo="x";
      $nombre="PRODUCTOS NO REGISTRADOS";
      $total=0;
      $i=0;

      $resultados2 = mysqli_query($conexion,"SELECT * from $tabla_db5 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
      while($consulta2 = mysqli_fetch_array($resultados2))
      {
        $id_factura=$consulta2['id'];

        //en la tabla de productos vendidos, relaciono los IDs de los productos 
        $resultados3 = mysqli_query($conexion,"SELECT * from $tabla_db4 WHERE codigo = '$codigo' AND id_factura = '$id_factura'");
        while($consulta3 = mysqli_fetch_array($resultados3))
        {
          //Dinero total recogido de ese producto
          $total=$total+$consulta3['precio_total'];
          //calculo la cantidad de productos que despacho
          $i=$i+$consulta3['cantidad'];
        } 
      }
      //El producto puede existir pero puede que no tenga ventas registradas
        if($total!=0)
        {
          $mensaje= 
          '
            <tr>
              <td><center>'.$i.'</center></td>
              <td>'.$nombre.'</td>
              <td>'.number_format($total, 0, ",", ".").'</td>
            </tr>
          ';
          echo $mensaje;
          echo "</table>";
        }


    }//Termina If verificando que existan datos
  }//Termina Isset verificando que exista la variable
?>