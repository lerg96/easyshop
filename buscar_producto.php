<?php
if (isset($_GET['term']))
{
	// conectare la base de datos
	include("abrir_conexion.php");

	$productos_mostrados = array(); //creamos el array que mostrara el input

	if ($conexion)
	{
		$resultados = mysqli_query($conexion,"SELECT * FROM $tabla_db2 WHERE nombre like '%" . mysqli_real_escape_string($conexion,($_GET['term'])) . "%' LIMIT 0 ,50");
		
		/* Recuperar y almacenar en conjunto los resultados de la consulta.*/
		while($consulta = mysqli_fetch_array($resultados))
		{
			
			//valor a mostrar en input
			$row_array['value'] = 		$consulta['descripcion'];
			
			$row_array['codigo']=	$consulta['codigo'];
			$row_array['descripcion']=	$consulta['descripcion'];
			$precio=number_format(		$consulta['precio'],0,",",".");
			$row_array['precio']=		$precio;
			$row_array['cantidad']=		"1";

			array_push($productos_mostrados,$row_array);
		}
	}

/* Cierra la conexión. */
mysqli_close($conexion);

/* Codifica el resultado del array en JSON. */
echo json_encode($productos_mostrados);

}
?>