<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Diseño y Publicidad" content="">
    <meta name="Central de Diseño" content="">
    <link rel="icon" href="img/logo.ico">
    <title>Informes</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-filestyle.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.numeric.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- Bootstrap core CSS -->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--JavaScrip para las FECHAS-->
    <script>
        $(function () {
            $('.datepicker').datepicker();
        });
    </script>

</head>
<body>
<?php
session_start();
ob_start();
include("abrir_conexion.php");

//Si no inicia sesion. ¡Chao papá!
if ($_SESSION['sesion_exito'] <> 1) {
    header('Location:index.php');
} //Si NO inicio sesion, ¡hasta luego!
if ($_SESSION['tipo_usuario'] <> "A") {
    header('Location:index.php');
}//Si NO es administrador, Chao mijo!


//Rectifico quien me llama (GET)
if (isset($_GET['tipo'])) {
    $tipo = $_GET['tipo'];
}

//Rectifico quien me llama (INGRESOS)
if (isset($_POST['informe_ingreso']) || $tipo == "i") {
    $tipo_informe = "INGRESOS";
}

//Rectifico quien me llama (EGRESOS)
if (isset($_POST['informe_egreso']) || $tipo == "e") {
    $tipo_informe = "EGRESOS";
}


//en caso de que me llame INGRESOS
if ($tipo_informe == "INGRESOS") {
    if ($tipo == "i") //Si vienen los datos por GET YA SE QUE VIENEN BIEN!
    {
        $fecha_inicial = $_GET['fecha_inicial'];
        $fecha_final = $_GET['fecha_final'];
        $del = $_GET['del'];
        $codigo = $_GET['codigo'];
        $error = $_GET['error'];

        if ($del == 1) {
            //Elimino PRIMERO todos los productos vendidos de la factura
            $_DELETE_SQL = "DELETE FROM $tabla_db4 WHERE id_factura = '$codigo'";
            mysqli_query($conexion, $_DELETE_SQL);

            //Elimino PRIMERO todos los productos vendidos de la factura
            $_DELETE_SQL = "DELETE FROM $tabla_db5 WHERE id = '$codigo'";
            mysqli_query($conexion, $_DELETE_SQL);
        }
    } else//Si no vienen por GET vienen por POST y toca arreglarlos
    {
        if ($_POST['fecha_inicial'] == "" || $_POST['fecha_final'] == "")//no especifico fechas, agrego la de HOY
        {
            $fecha_inicial = date('m/d/Y');
            $fecha_final = date('m/d/Y');
        } else//Envian fechas desde Admin
        {
            $fecha_inicial = $_POST['fecha_inicial'];
            $fecha_final = $_POST['fecha_final'];
        }

        //Configuro las fechas para adaptarlas a SQL (año-mes-dia)
        list($mes, $dia, $year) = explode("/", $fecha_inicial);
        $fecha_inicial = $year . "-" . $mes . "-" . $dia;

        list($mes, $dia, $year) = explode("/", $fecha_final);
        $fecha_final = $year . "-" . $mes . "-" . $dia;
    }


    //calculo el total de todos los ingresos entre las fechas
    $resultados = mysqli_query($conexion, "SELECT * from $tabla_db5 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
    while ($consulta = mysqli_fetch_array($resultados)) {
        $total_movimiento = $total_movimiento + $consulta['total'];
        $ganancia_movimiento = $ganancia_movimiento + $consulta['ganancia'];
    }


    //calculo el total de todos los egresos entre las fechas
    $resultados = mysqli_query($conexion, "SELECT * from $tabla_db6 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
    while ($consulta = mysqli_fetch_array($resultados)) {
        $total_egresos = $total_egresos + $consulta['total'];
    }
}

//calculo el informe de los EGRESOS
if ($tipo_informe == "EGRESOS") {
    if ($tipo == "e") //Si vienen los datos por GET YA SE QUE VIENEN BIEN!
    {
        $fecha_inicial = $_GET['fecha_inicial'];
        $fecha_final = $_GET['fecha_final'];
        $del = $_GET['del'];
        $codigo = $_GET['codigo'];

        if ($del == 1) {
            $_DELETE_SQL = "DELETE FROM $tabla_db6 WHERE id = '$codigo'";
            mysqli_query($conexion, $_DELETE_SQL);
        }
    } else//Si no vienen por GET vienen por POST y toca arreglarlos
    {
        if ($_POST['fecha_inicial'] == "" || $_POST['fecha_final'] == "")//no especifico fechas, agrego la de HOY
        {
            $fecha_inicial = date('m/d/Y');
            $fecha_final = date('m/d/Y');
        } else//Envian fechas desde Admin
        {
            $fecha_inicial = $_POST['fecha_inicial'];
            $fecha_final = $_POST['fecha_final'];
        }

        //Configuro las fechas para adaptarlas a SQL (año-mes-dia)
        list($mes, $dia, $year) = explode("/", $fecha_inicial);
        $fecha_inicial = $year . "-" . $mes . "-" . $dia;

        list($mes, $dia, $year) = explode("/", $fecha_final);
        $fecha_final = $year . "-" . $mes . "-" . $dia;
    }


    //calculo el total de todos los ingresos entre las fechas
    $resultados = mysqli_query($conexion, "SELECT * from $tabla_db6 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
    while ($consulta = mysqli_fetch_array($resultados)) {
        $total_movimiento = $total_movimiento + $consulta['total'];
    }
}
?>
<div class="container">
    <input type="hidden" name="agregar_producto" id="existe" value="1">
    <input type="hidden" name="no_mesa" id="mesa" value="<?php echo $mesa; ?>">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>INFORME GENERADO</h1>
            <p class="lead"><strong>INFORMES DE <?php echo $tipo_informe; ?></strong></p>
            <hr>
        </div>
    </div>


    <h3>
        <center><strong>
                <p class="bg-success">
                    <?php
                    if ($error == 1) {
                        echo 'FACTURA CON ID:' . $codigo . ' ELIMINADA CON EXITO';
                    }
                    ?>
                </p>
            </strong></center>
    </h3>


    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="well">

                <center>
                    <h2><strong>
                            <?php
                            if ($tipo_informe == "INGRESOS") {
                                echo 'TOTAL INGRESOS<br>' . number_format($total_movimiento, 0, ",", ".") . '<br>';
                                echo 'TOTAL GANANCIA<br>' . number_format($ganancia_movimiento, 0, ",", ".") . '<br>';
                                echo '<h3><b>EGRESOS</b><br>' . number_format($total_egresos, 0, ",", ".") . '</h3>
                <h6>*Total de egresos en la misma fecha</h6>';
                            } else {
                                echo 'TOTAL EGRESOS<br>' . number_format($total_movimiento, 0, ",", ".");
                            }

                            echo '</strong></h2>';

                            if ($tipo_informe == "INGRESOS") {
                                echo '
              <a href="exportar/exportar.php?tipo=I&fecha_inicial=' . $fecha_inicial . '&fecha_final=' . $fecha_final . '" class="btn btn-success btn-lg" role="button">EXPORTAR</a>
              ';
                            } else {
                                echo '
              <a href="exportar/exportar.php?tipo=E&fecha_inicial=' . $fecha_inicial . '&fecha_final=' . $fecha_final . '" class="btn btn-success btn-lg" role="button">EXPORTAR</a>
              ';
                            }
                            ?>
                            <a href="admin.php" class="btn btn-warning btn-lg" role="button">VOLVER</a>
                </center>

            </div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h2>
                <center>DETALLES</center>
            </h2>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" width="100%">

                    <?php
                    if ($tipo_informe == "INGRESOS") {
                        echo
                        '
              <tr>
                <th width="10%"><center>ID</center></th>
                <th width="20%"><center>FECHA</center></th>
                <th width="30%"><center>RESPONSABLE</center></th>
                <th width="10%"><center>TOTAL</center></th>
                <th width="10%"><center>DETALLES</center></th>
                <th width="10%"><center>IMPRIMIR</center></th>
                
              </tr>
            ';
                        $resultados = mysqli_query($conexion, "SELECT * from $tabla_db5 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
                        while ($consulta = mysqli_fetch_array($resultados)) {
                            echo '
                <tr>
                  <td><center>' . $consulta['id'] . '</center></td>
                  <td><center>' . $consulta['fecha'] . '</center></td>
                  <td><center>' . $consulta['responsable'] . '</center></td>
                  <td><center>' . number_format($consulta['total'], 0, ",", ".") . '</center></td>
                  <td><center><a href="informe_factura.php?codigo=' . $consulta['id'] . '&responsable=' . $consulta['responsable'] . '&fecha_inicial=' . $fecha_inicial . '&fecha_final=' . $fecha_final . '"><img src="img/mas.png" width="20" height="20"></a></center></td>

                  <td><center><a target="_blank" href="generar_factura.php?id_factura=' . $consulta['id'] . '"><img src="img/imprimir2.png" width="20" height="20"></a></center></td>
                </tr>';
                        }
                    }
                    if ($tipo_informe == "EGRESOS") {
                        echo
                        '
              <tr>
                <th width="5%"><center>ID</center></th>
                <th width="15%"><center>Fecha</center></th>
                <th width="45%"><center>Descripción</center></th>
                <th width="25%"><center>Responsable</center></th>
                <th width="10%"><center>Total</center></th>
                <th width="5%"><center>¿Borrar?</center></th>
              </tr>
            ';
                        $resultados = mysqli_query($conexion, "SELECT * from $tabla_db6 WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final'");
                        while ($consulta = mysqli_fetch_array($resultados)) {
                            echo '
                <tr>
                  <td><center>' . $consulta['id'] . '</center></td>
                  <td><center>' . $consulta['fecha'] . '</center></td>
                  <td><center>' . $consulta['descripcion'] . '</center></td>
                  <td><center>' . $consulta['responsable'] . '</center></td>
                  <td><center>' . number_format($consulta['total'], 0, ",", ".") . '</center></td>
                  <td><center><a href="generar_informe.php?tipo=e&del=1&codigo=' . $consulta['id'] . '&fecha_inicial=' . $fecha_inicial . '&fecha_final=' . $fecha_final . '"><img src="img/x.png" width="20" height="20"></a></center></td>
                </tr>';
                        }
                    }
                    ?>
                </table>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</div><!--Containder-->
<?php include("cerrar_conexion.php"); ?>
</body>

</html>
