<html>
<!DOCTYPE html>
<!-- saved from url=(0050)http://getbootstrap.com/examples/navbar-fixed-top/ -->
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Diseño y Publicidad" content="">
    <meta name="Central de Diseño" content="">
    <link rel="icon" href="img/logo.ico">
    <title>Ventas</title>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <!--<link href="./css/navbar-fixed-top.css" rel="stylesheet">-->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="./js/ie-emulation-modes-warning.js"></script>
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="./js/ie10-viewport-bug-workaround.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--Calculo los totales de la hoja-->
</head>
<body>
<?php
session_start();
ob_start();
include("abrir_conexion.php");

//Si no inicia sesion. ¡Chao papá!
if ($_SESSION['sesion_exito'] <> 1) {
    header('Location:index.php');
} //Si NO inicio sesion, ¡hasta luego!
if ($_SESSION['tipo_usuario'] <> "A") {
    header('Location:index.php');
}//Si NO es administrador, Chao mijo!

$error = 0;

if(isset($_GET['codigo'])){
    $codigo = $_GET['codigo'];
    $resultados = mysqli_query($conexion, "SELECT * FROM $tabla_db2 WHERE codigo = $codigo");
    while ($consulta = mysqli_fetch_array($resultados)) {
        $codigo = $consulta['codigo'];
        $nombre = $consulta['nombre'];
        $precio = $consulta['precio'];
        $precio_compra = $consulta['precio_compra'];
        $cantidad = $consulta['cantidad'];

    }
}

?>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1>EDITAR PRODUCTO</h1>
            <p class="lead"><strong>Edición de datos de un producto</strong></p>
            <hr>
        </div>
    </div>

    <h3>
        <center><strong>
                <?php
                echo '<p class="bg-danger">';
                if ($error == "3") {
                    echo "ERROR, TODOS LOS DATOS SON OBLIGATORIOS";
                }
                if ($error == "4") {
                    echo "ERROR, LA IMAGEN ES MUY GRANDE (MAXIMO 2MB)";
                }
                if ($error == "5") {
                    echo "ERROR, FORMATO DE IMAGEN NO PERMITIDO";
                }
                echo '</p>';
                echo '<p class="bg-success">';
                if ($error == "1") {
                    echo "PRODUCTO GUARDADO CON EXITO";
                }
                if ($error == "2") {
                    echo "PRODUCTO ELIMINADO CON EXITO";
                }
                echo '</p>';
                ?>
            </strong></center>
    </h3>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="well">
                <form method="POST" action="guardar_producto_editable.php" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="producto">PRODUCTO</label>
                        <input type="text" class="form-control" id="producto" placeholder="Nombre Corto"
                               name="producto" value="<?php echo $nombre; ?>">
                    </div>
                    <div class="form-group">
                        <label for="precio">PRECIO DE VENTA</label>
                        <input type="text" class="form-control" id="precio" placeholder="Precio sin puntos"
                               name="precio" value="<?php echo $precio; ?>">
                    </div>
                    <div class="form-group">
                        <label for="precio_compra">PRECIO DE COMPRA</label>
                        <input type="text" class="form-control" id="precio_compra" placeholder="Precio sin puntos"
                               name="precio_compra" value="<?php echo $precio_compra; ?>">
                    </div>
                    <div class="form-group">
                        <label for="cantidad">CANTIDAD</label>
                        <input type="text" class="form-control" id="cantidad" placeholder="Cantidad sin puntos"
                               name="cantidad" value="<?php echo $cantidad; ?>">
                    </div>
                    <input type="hidden" value="<?php echo $codigo; ?>" name="codigo">
                    <hr>
                    <center>
                        <button type="submit" class="btn btn-success btn-lg" name="guardar_producto">GUARDAR</button>
                        <a href="agregar_productos.php" class="btn btn-warning btn-lg" role="button">VOLVER</a>
                    </center>
                </form>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div><!--Containder-->
<?php include("cerrar_conexion.php"); ?>
</body>

</html>
